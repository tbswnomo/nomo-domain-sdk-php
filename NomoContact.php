<?php
 
include_once "nomo_server_constants.php";
 
   // Copyright (c) 2014. Togglebolt Software.
   // genereated by C:\ToggleboltSW\Products\NoMo\NoMo-Tools\SDK\generateNomoCart.php on UTC: 2014/11/19 19:44:27
 
class NomoContact {
 
    private $Fname = "";
    private $Lname = "";
    private $Phone = "";
    private $Email = "";

    private $valid = "true";

 
 
    public function __construct($nomo_json) {
        $this->fromNomoJson($nomo_json);
        return;
    }
    public function isValid(){

       return true;

    }

    public function setFname($Fname){
        $this->Fname = trim($Fname);
    }

    public function getFname(){
       return $this->Fname;
    }
    public function setLname($Lname){
        $this->Lname = trim($Lname);
    }

    public function getLname(){
       return $this->Lname;
    }
    public function setPhone($Phone){
        $this->Phone = trim($Phone);
    }

    public function getPhone(){
       return $this->Phone;
    }
    public function setEmail($Email){
        $this->Email = trim($Email);
    }

    public function getEmail(){
       return $this->Email;
    }

 
    public function toNoMoJson($enclosingBraces){
        $result = "";
        try{
            if ($enclosingBraces)
                $result .= "{";
 
            $result .= " ".'"'.NOMO_FIELD_CONTACT_NAME_FIRST.'":'.json_encode($this->Fname).'';
            $result .= ",".'"'.NOMO_FIELD_CONTACT_NAME_LAST.'":'.json_encode($this->Lname).'';
            $result .= ",".'"'.NOMO_FIELD_CONTACT_PHONE.'":'.json_encode($this->Phone).'';
            $result .= ",".'"'.NOMO_FIELD_CONTACT_EMAIL.'":'.json_encode($this->Email).'';

            if ($enclosingBraces)
                $result .= "}";
        }catch (Exception $ex){
        }
        return $result;
   }
   public function fromNomoJson($nomo_field_contact){
       try{
           //Passed contents of the NOMO_FIELD_CHECKOUT_DATA_CONTACT field 
           if (!isset($nomo_field_contact)){
               return;
           }
           if (empty($nomo_field_contact)){ 
               return;
           }
 
            $this->Fname = $this->extractField($nomo_field_contact,NOMO_FIELD_CONTACT_NAME_FIRST,"");
            $this->Lname = $this->extractField($nomo_field_contact,NOMO_FIELD_CONTACT_NAME_LAST,"");
            $this->Phone = $this->extractField($nomo_field_contact,NOMO_FIELD_CONTACT_PHONE,"");
            $this->Email = $this->extractField($nomo_field_contact,NOMO_FIELD_CONTACT_EMAIL,"");

       }catch(Exception $ex){
 
       }
       return;
    }
 
    public function extractField ($nomoJson, $fieldConstant, $defaultValue){
        try{
            $data = null;
            if (is_string($nomoJson)){
                $data = json_decode($nomoJson, true);
            }else{
                if (is_array($nomoJson))
                    $data = $nomoJson;
            }
            if (!isset($data))
                return $defaultValue;
     
            if(isset( $data[$fieldConstant] ) ){
                $tmp = json_encode($data[$fieldConstant]);
                $tmp1 = trim($tmp, '"'); 
                return $tmp1;
            }
            return $defaultValue;
        }catch(Exception $ex){
            return $defaultValue;
        }
    }
}

?>

