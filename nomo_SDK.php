<?php
include_once dirname(__FILE__).DIRECTORY_SEPARATOR."nomo_SDK_configuration.php";
include_once dirname(__FILE__).DIRECTORY_SEPARATOR."nomo_server_constants.php";
include_once dirname(__FILE__).DIRECTORY_SEPARATOR."nomo_logger.php";

class NOMO_SDK{
    public $status = "";
    public $userAlias = "";
    public $issuer = "";
    public $introductionCode = "";
    public $transactionId = "";
    public $deviceId = "";
    public $qrCodeUrl = "";
    public $serverURL = "";

    public function NOMO_FETCH_DOMAIN_CONFIG(){
        $response = "";
        try{
            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Starting");

            $clientIp = $_SERVER['REMOTE_ADDR'];

            $this->status = NOMO_STATUS_AJAX_ERROR;
            $requestData = $this->getRequestConfigJson(MY_DOMAIN, $clientIp);
            $this->serverURL =  $this->getNomoServerURL(NOMO_MASTER_SERVER_URL,NOMO_WS_URL_DOMAIN_REQUEST_CONFIG);
            $response = $this->callNOMOWebservice($this->serverURL, $requestData, null);
            if (isset($response) && !empty($response)){
                $jsonarray = json_decode($response, true);
                $this->status = $jsonarray[NOMO_FIELD_STATUS];
                if ($this->status == NOMO_STATUS_SUCCESS ){
                    $this->issuer = $jsonarray[NOMO_FIELD_NOMO_ISSUER];
                }
            }else{
                Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"No Response");
                $response = '{"status":'.NOMO_STATUS_AJAX_ERROR.',"URL":"'.$this->serverURL.'","Error":"No Response"}';
            }
        }catch (Exception $e){
            $response = '{"status":'.NOMO_STATUS_AJAX_ERROR.',"URL":"'.$this->serverURL.'","Exception":'.$e.'}';
        }

        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Response: ".$response);
        return $response;
    }

    public function NOMO_FETCH_NEW_TRANSACTION(){
        $response = "";
        try{
            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Starting");

            $clientIp = $_SERVER['REMOTE_ADDR'];

            //Call the nomo server to get the QR code
            $this->status = NOMO_STATUS_AJAX_ERROR;
            $requestData = $this->getQRCodeRequestJson(MY_DOMAIN, $clientIp);
            $this->serverURL =  $this->getNomoServerURL($this->issuer,NOMO_WS_URL_DOMAIN_REQUEST_INTRODUCTION);
            $response = $this->callNOMOWebservice($this->serverURL, $requestData, null);
            if (isset($response) && !empty($response)){
                $jsonarray = json_decode($response, true);
                $this->status = $jsonarray[NOMO_FIELD_STATUS];
                if ($this->status == NOMO_STATUS_SUCCESS ){
                    $this->transactionId = $this->NOMO_GET_FIELD($response,NOMO_FIELD_TRANSACTION_ID,"");
                    $this->qrCodeUrl = $this->NOMO_GET_FIELD($response,NOMO_FIELD_IMAGE_FILE_URL,"");
                    $this->introductionCode = $this->NOMO_GET_FIELD($response, NOMO_FIELD_INTRODUCTION_CODE, "");
                }
            }else{
                Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"No Response");
                $response = '{"status":'.NOMO_STATUS_SERVER_ERROR.',"URL":"'.$this->serverURL.'","Error":"No Response"}';
            }
        }catch (Exception $e){
            $response = '{"status":'.NOMO_STATUS_SERVER_ERROR.',"URL":"'.$this->serverURL.'","Exception":'.$e.'}';
        }
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Response: ".$response);
        return $response;
    }

    public function NOMO_REQUEST_INTRODUCTION($processUrl, $expiredUrl, $errorUrl, $inCheckout=false){
        try{
            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Starting");

            $code = "\n<!--NOMO Integration Begin: -->"."\n";

            $response = $this->NOMO_FETCH_DOMAIN_CONFIG();
            if ($this->status != NOMO_STATUS_SUCCESS){
                $url =  $errorUrl."?".NOMO_FIELD_AJAX_ACTION.'="Fetch_Domain_Config"';
                $url .= "&".NOMO_FIELD_STATUS."=".$this->status;
                $url .= "&".NOMO_FIELD_AJAX_URL."=".$this->serverURL;
                header("Location:".$url);
                exit;
            }

            $code .= "\n<!--NOMO Integration: Issuer Base URL ($this->issuer) -->\n";

            $response = $this->NOMO_FETCH_NEW_TRANSACTION();
            $code .= "\n<!--NOMO Integration: Transaction Response ($response) -->\n";

            if ($this->status == NOMO_STATUS_SUCCESS){
                if (!empty($this->transactionId) &&
                    !empty($this->qrCodeUrl)){
                    $code .= "\n<!--NOMO Integration QR Block: -->"."\n";
                    $code .= "   <div class='nomo_integrtion'>"."\n";
                    if (isset($this->introductionCode) && !empty($this->introductionCode)){
                        $code .= "      <div class='nomo_introduction_code'>"."\n";
                        $code .= "          <h3>".$this->introductionCode."</h3>";
                        $code .= '      </div> <!-- End of nomo_introduction_code -->'."\n";
                    }
                    $code .= "      <div class='nomo_qr_code'>"."\n";
                        $code = "<br><!-- ".NOMO_SDK_DIRECTORY."images".DIRECTORY_SEPARATOR."mobile_Login_Icon.png --><br>";
                        if ($this->isMobileBrowser()){
                            $buttonImg = NOMO_SDK_DIRECTORY."images".DIRECTORY_SEPARATOR."mobile_Login_Icon.png";
                            $code .= '         <a href="nomo://nomo.auth.com/';
                            $code .= '            ?'.NOMO_FIELD_EXTERNAL_TASK_ID.'='.NOMO_FIELD_VALUE_TASK_ID_LOGIN;
                            $code .= '            &'.NOMO_FIELD_DOMAIN.'='.MY_DOMAIN;
                            $code .= '            &'.NOMO_FIELD_TRANSACTION_ID."=".$this->transactionId;
                            $code .= '            &'.NOMO_FIELD_NOMO_ISSUER."=".$this->issuer;
                            $code .= '            &'.NOMO_FIELD_DOMAIN_OPTION_FAST_CHECKOUT."=$inCheckout";
                            $code .= '         ">';
                            $code .= '            <img class="nomo_introduction_button" src="'.$buttonImg.'">';
                            $code .= '         </a>'."\n";
                        }else{
                            $code .= '         <img class="nomo_introduction_qr_code" src="'.$this->qrCodeUrl.'">'."\n";
                        }
                    $code .= '      </div> <!-- End of nomo_qr_code -->'."\n";

                    $code .= "      <div class='nomo_deviceid_form'>"."\n";
                    $code .= '         <form action="javascript:NOMO_Auth_Request();">'."\n";
                    $code .= "            <div class='nomo_device_id'>"."\n";
                    $code .= '               NOMO Login Id: <input type="text" placeholder="NOMO Login Id" id="nomo_login" value="">'."\n";
                    $code .= '               <input type="submit" value="submit">'."\n";
                    $code .= '            </div>'."\n";
                    $code .= '         </form>'."\n";
                    $code .= '      </div> <!-- End of nomo_deviceid_form -->'."\n";

                    $code .= '   </div> <!-- End of nomo_integrtion -->'."\n";
                    $code .= $this->buildLoginSnippet($this->issuer, MY_DOMAIN, $this->transactionId, $processUrl, $expiredUrl, $errorUrl);
                    $code .= '<!--NOMO Integration End. -->'."\n";
                }else{
                    Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"NOMO Integration: Unsuccessful response ($response)");
                    $code = $code."\n<!--NOMO Integration: Unsuccessful response ($response). -->\n";
                }
            }else{
                Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Error encountered response ($response)");
                $url =  $errorUrl."?".NOMO_FIELD_AJAX_ACTION."='Fetch_New_Transaction'";
                $url .= '&'.NOMO_FIELD_STATUS.'='.$this->status;
                $url .= "&".NOMO_FIELD_AJAX_URL."=".$this->serverURL;
                header("Location:".$url);
                exit;
            }

            //For ease of integration echo the result directly to the output stream.
            echo $code;
        }catch (Exception $e){
            Logger::getInstance()->LogFatal(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"exception ".$e->getMessage());
            Logger::getInstance()->LogFatal(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"exception Trace".$e->getTraceAsString());
            echo "\n<!--Exception encountered ($e). -->\n";
        }
    }

    public function NOMO_FETCH_INTRODUCTION($nomoServer, $transactionId){
        $result = "result not available.";
        try{
            $this->transactionId = $transactionId;
            $this->issuer = $nomoServer;

            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Starting");

            $this->status = NOMO_STATUS_AJAX_ERROR;
            if (empty($transactionId)){
                return $result;
            }
            $domain = MY_DOMAIN;
            $loginJson = $this->getAuthorizationRequestJson($this->issuer, $domain, $_SERVER['REMOTE_ADDR'], $this->transactionId);
            $url =  $this->getNomoServerURL($this->issuer,NOMO_WS_URL_DOMAIN_FETCH_INTRODUCTION);
            $response = $this->callNOMOWebservice($url, $loginJson, null);
            if (isset($response)){
                $json = json_decode($response, true);
                if (isset($json[NOMO_FIELD_STATUS])){
                    $this->status =  $json[NOMO_FIELD_STATUS];
                }

                if (isset($json[NOMO_FIELD_PAYLOAD])){
                    $payload = $this->decryptPayload($json[NOMO_FIELD_PAYLOAD]);
                    $payload[NOMO_FIELD_STATUS] = $this->status;
                    $result = $payload;
                }else{
                    $result = $response;
                }
                if ($this->status == NOMO_STATUS_SUCCESS){
                    $this->deviceId = $this->NOMO_GET_FIELD($result,NOMO_FIELD_DEVICE_ID,"");
                    $this->userAlias = $this->NOMO_GET_FIELD($result,NOMO_FIELD_DEVICE_ALIAS,"");
                }
            }else{
                $result = '{"status":'.NOMO_STATUS_AJAX_ERROR.'}';
            }
        }catch (Exception $e){
            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"exception: ".$e->getMessage());
            $result = '{"status":'.NOMO_STATUS_SERVER_ERROR.',"Exception":'.$e.'}';
        }



        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Result: ".json_encode($result));

        return $result;
    }

    public function NOMO_ACCEPT_INTRODUCTION($nomoServer, $transactionId){
        //PHP code to record the user login for a NOMO transaction
        //Transaction ID was included in the HTTP URL as a parameter by the NOMO JavaScript code for the login page.
        $this->transactionId = $transactionId;
        $this->issuer = $nomoServer;

        $this->status = NOMO_STATUS_SERVER_ERROR;
        $response = "";
        try{
            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Starting");

            if (empty($this->transactionId)){
                return NOMO_STATUS_TRANSACTION_ID_MISSING;
            }

            $loginJson = $this->getLoginRequestJson(MY_DOMAIN, $_SERVER['REMOTE_ADDR'], $this->transactionId);
            $url =  $this->getNomoServerURL($nomoServer,NOMO_WS_URL_DOMAIN_ACCEPT_INTRODUCTION);
            $response = $this->callNOMOWebservice($url, $loginJson, null);
            if (isset($response) && !empty($response)){
                $json = json_decode($response, true);
                Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"parsing status");
                if (isset($json[NOMO_FIELD_STATUS])){
                    $this->status = $json[NOMO_FIELD_STATUS];
                    Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"setting status to ".$this->status);
                }
            }
        }catch (Exception $e){
            $response = '{"status":'.NOMO_STATUS_SERVER_ERROR.',"Exception":'.$e.'}';
        }
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"response ($response)");

        return $response;
    }

    public function NOMO_FETCH_SHIPPING_LOCALS($nomoServer, $transactionId, $deviceId){
        $response = "";
        try{
            $this->issuer = $nomoServer;
            $this->transactionId = $transactionId;
            $this->deviceId = $deviceId;
            $this->status = NOMO_STATUS_AJAX_ERROR;

            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Starting");

            $clientIp = $_SERVER['REMOTE_ADDR'];

            $requestData = $this->getRequestShippingLocalsJson(MY_DOMAIN, $clientIp, $deviceId);
            $this->serverURL =  $this->getNomoServerURL(NOMO_MASTER_SERVER_URL,NOMO_WS_URL_DOMAIN_REQUEST_SHIPPING_LOCALS);
            $response = $this->callNOMOWebservice($this->serverURL, $requestData, null);
            if (isset($response) && !empty($response)){
                $jsonarray = json_decode($response, true);
                $this->status = $jsonarray[NOMO_FIELD_STATUS];
            }else{
                Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"No Response");
                $response = '{"status":'.NOMO_STATUS_AJAX_ERROR.',"URL":"'.$this->serverURL.'","Error":"No Response"}';
            }
        }catch (Exception $e){
            $response = '{"status":'.NOMO_STATUS_AJAX_ERROR.',"URL":"'.$this->serverURL.'","Exception":'.$e.'}';
        }

        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Response: ".$response);
        return $response;
    }

    public function NOMO_CHECKOUT(
        $nomoServer,
        $transactionId,
        $deviceId,
        nomoCart $shoppingCart,
        $shippingOptions = array(),
        $requiredData=array())
    {

        $response = "";
        try{
            $this->issuer = $nomoServer;
            $this->transactionId = $transactionId;
            $this->deviceId = $deviceId;
            $this->status = NOMO_STATUS_AJAX_ERROR;

            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"starting for Device Id: ".$deviceId);

            //Call the nomo server to get the checkout payment and shipping data from the user.
            $clientIp =  $_SERVER['REMOTE_ADDR'];

            $performFastCheckout = false;
            if ((!isset($this->issuer) || empty($this->issuer)) &&
                (!isset($this->transactionId) || empty($this->transactionId))){
                if (isset($this->deviceId) && !empty($this->deviceId)){
                    Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"performing FastCheckout");
                    $performFastCheckout = true;
                }
            }

            if ($performFastCheckout){
                $response = $this->NOMO_FETCH_DOMAIN_CONFIG();
                if ($this->status == NOMO_STATUS_SUCCESS){
                    $response = $this->NOMO_FETCH_NEW_TRANSACTION();
                    if ($this->status != NOMO_STATUS_SUCCESS){
                        return $response;
                    }
                }else{
                    return $response;
                }
            }

            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext()," Processing for Device: ".$deviceId);

            $shoppingCart->updateTotals();
            $requestData = $this->getCheckoutRequestJson(
                MY_DOMAIN,
                $clientIp,
                $deviceId,
                $this->transactionId,
                $shoppingCart,
                $shippingOptions,
                $requiredData);
            $url =  $this->getNomoServerURL($this->issuer,NOMO_WS_URL_DOMAIN_REQUEST_CHECKOUT);
            $response = $this->callNOMOWebservice($url, $requestData, null);
            $payload = "";
            if (isset($response)){
                $jsonarray = json_decode($response, true);
                $this->status = $jsonarray[NOMO_FIELD_STATUS];
                if ($this->status == NOMO_STATUS_SUCCESS){
                    $url =  $this->getNomoServerURL($this->issuer, NOMO_WS_URL_DOMAIN_POLL_CHECKOUT);
                    $requestData = $this->getCheckoutPollRequestJson(MY_DOMAIN, $clientIp, $this->transactionId);

                    $start = strtotime(date('Y/m/d H:i:s'));
                    $alive = 0;
                    while ($alive < CHECKOUT_POLL_WAIT){
                        $response = $this->callNOMOWebservice($url, $requestData, null);
                        if (isset($response)){
                            $jsonarray = json_decode($response, true);
                            $this->status = $jsonarray[NOMO_FIELD_STATUS];
                            switch ($this->status){
                                Case NOMO_STATUS_POLL_DECLINED;
                                    Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Poll Declined");
                                    break 2;
                                case NOMO_STATUS_POLL_DATA_AVAILABLE:
                                    Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Poll Accepted");
                                    $data = $this->NOMO_GET_FIELD($jsonarray, NOMO_FIELD_PAYLOAD, "");
                                    $payload = $this->decryptPayload($data);
                                    Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Poll Data decrypted (".json_encode($payload).")");
                                    $this->status = NOMO_STATUS_SUCCESS;
                                    break 2;
                                case NOMO_STATUS_POLL_DATA_NOT_AVAILABLE:
                                    Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Poll Data not available yet");
                                    break;
                                default:
                                    Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Error Status ($this->status)");
                                    break 2;
                            }
                        }
                        //Pause for a few milli seconds and check again
                        sleep(1);
                        $alive = strtotime(date('Y/m/d H:i:s')) - $start;
                    }
                }else{
                    Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"return Error Response ($response)");
                    return $response;
                }
            }
            if ($this->status == NOMO_STATUS_SUCCESS){
                $response = '{';
                $response .= '"'.NOMO_FIELD_STATUS.'":'.$this->status;
                if (isset($payload)){
                    $response .= ',"'.NOMO_FIELD_PAYLOAD.'":'.json_encode($payload);
                }
                $response .= '}';
            }else{
                //Not success just return the response
            }
        }catch (Exception $e){
            $response = '{"status":'.NOMO_STATUS_SERVER_ERROR.',"Exception":'.$e.'}';
        }
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"return Response: ".json_encode($response));
        return $response;

    }

    public function NOMO_CHECKOUT_COMPLETE($status, nomoCart $cart){
        $result = "";
        try{
            Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"starting for Device Id: ".$this->deviceId);

            $requestData = $this->getCheckoutCompleteRequestJson(
                MY_DOMAIN,
                $_SERVER['REMOTE_ADDR'],
                $this->deviceId,
                $this->transactionId,
                $status,
                $cart);
            $url = $this->getNomoServerURL($this->issuer,NOMO_WS_URL_DOMAIN_CHECKOUT_COMPLETE);
            $response = $this->callNOMOWebservice($url, $requestData, null);
            if (isset($response)){
                $jsonarray = json_decode($response, true);
                $this->status = $jsonarray[NOMO_FIELD_STATUS];
            }else{
                //NO Response from Web Call.
                $this->status = NOMO_STATUS_REQUEST_ERROR;
            }
            $result = '{';
            $result .= '"'.NOMO_FIELD_STATUS.'":'.$this->status;
            $result .= '}';
        }catch (Exception $e){
            $result = '{"status":'.NOMO_STATUS_SERVER_ERROR.',"Exception":'.$e.'}';
        }

        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"return Response ($result)");
        return $result;
    }

    public function NOMO_GET_CONTACT($checkoutResponse){
        $result = null;
        try{
            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"starting for Device Id: ".$this->deviceId);

            $checkout = $this->getCheckoutJson($checkoutResponse);
            if (!isset($checkout[NOMO_FIELD_CHECKOUT_DATA_CONTACT])){
                Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Missing ".NOMO_FIELD_CHECKOUT_DATA_CONTACT." field");
                return $result;
            }

            $tmp = json_encode($checkout[NOMO_FIELD_CHECKOUT_DATA_CONTACT]);
            Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Contact Data ($tmp)");
            $result = new NomoContact($tmp);
        }catch (Exception $e){
            Logger::getInstance()->LogFatal(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Exception ($e)");
        }
        return $result;
    }

    public function NOMO_GET_SHIPPING_LOCATIONS($response){
        $result = array();
        try{
            $data = json_decode($response,true);

            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"starting with: ".$response);
            if (!isset($data[NOMO_FIELD_PAYLOAD])) {
                Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Missing ".NOMO_FIELD_PAYLOAD." field");
                return $result;
            }


            $payload = $data[NOMO_FIELD_PAYLOAD];
            if (!isset($payload[NOMO_FIELD_MERCHANT_SHIPPING_OPTIONS])){
                Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Missing ".NOMO_FIELD_MERCHANT_SHIPPING_OPTIONS." field");
                return $result;
            }

            $locals = $payload[NOMO_FIELD_MERCHANT_SHIPPING_OPTIONS];

            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Locals: ".json_encode($locals));

            foreach($locals as $local){
                $country = $local[NOMO_FIELD_MERCHANT_SHIPPING_OPTION_COUNTRY];
                $postal = $local[NOMO_FIELD_MERCHANT_SHIPPING_OPTION_POSTAL];

                if (empty($country) || empty($postal)){
                    continue;
                }
                $data = new NomoShippingOption();
                $data->setScountry($country);
                $data->setSpostal($postal);
                array_push($result, $data);
            }
            Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Locations Count: ". sizeof($result));

        }catch (Exception $e){
            Logger::getInstance()->LogFatal(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Exception ($e)");
        }
        return $result;
    }

    public function NOMO_GET_PAYMENT($checkoutResponse){
        $result = null;
        try{
            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"starting");

            $checkout = $this->getCheckoutJson($checkoutResponse);

            if (!isset($checkout[NOMO_FIELD_CHECKOUT_DATA_PAYMENT])){
                Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Missing ".NOMO_FIELD_CHECKOUT_DATA_PAYMENT." field");
                return $result;
            }
            $tmp = json_encode($checkout[NOMO_FIELD_CHECKOUT_DATA_PAYMENT]);

            Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"payment Data ($tmp)");
            $result = new NomoPayment($tmp);
        }catch (Exception $e){
            Logger::getInstance()->LogFatal(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Exception ($e)");
        }
        return $result;
    }

    public function NOMO_GET_SHIPPING($checkoutResponse){
        $result = null;
        try{
            Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"starting");

            $checkout = $this->getCheckoutJson($checkoutResponse);

            if (!isset($checkout[NOMO_FIELD_CHECKOUT_DATA_SHIPPING])){
                Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Missing ".NOMO_FIELD_CHECKOUT_DATA_PAYMENT." field");
                return $result;
            }
            $shipping = json_encode($checkout[NOMO_FIELD_CHECKOUT_DATA_SHIPPING]);
            Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Address Data ($shipping)");
            $result = new NomoShipping($shipping);
        }catch (Exception $e){
            Logger::getInstance()->LogFatal(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Exception ($e)");
        }
        return $result;
    }

    public function NOMO_GET_FIELD ($nomoJson, $fieldConstant, $defaultValue){
        try{
            $data = null;
            if (is_string($nomoJson)){
                $data = json_decode($nomoJson, true);
            }else{
                if (is_array($nomoJson))
                    $data = $nomoJson;
            }
            if (!isset($data)) {
                Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"No Input data");
                return $defaultValue;
            }

            if(isset( $data[$fieldConstant] ) ){
                return ($data[$fieldConstant]);
            }else{
                Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Missing Field: ".$fieldConstant);
            }
            return $defaultValue;
        }catch(Exception $ex){
            Logger::getInstance()->LogFatal(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Exception ($ex)");
            return $defaultValue;
        }
    }

    private function getCheckoutJson($rawdata){
        $result = "{}";
        try{
            if (is_string($rawdata)){
                $data = json_decode($rawdata, true);
            }else{
                $data = $rawdata;
            }

            Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext()," Data: ".json_encode($data));

            if (!isset($data[NOMO_FIELD_PAYLOAD])) {
                Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Missing ".NOMO_FIELD_PAYLOAD." field");
                return $result;
            }
            $payload = $data[NOMO_FIELD_PAYLOAD];

            if (!isset($payload[NOMO_FIELD_CHECKOUT_DATA])){
                Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Missing ".NOMO_FIELD_CHECKOUT_DATA." field");
                return $result;
            }
            $result = $payload[NOMO_FIELD_CHECKOUT_DATA];
            Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Checkout Data: ".json_encode($result));

        }catch (Exception $e){
            Logger::getInstance()->LogFatal(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Exception ($e)");
        }
        return $result;

    }

    private function buildLoginSnippet($nomoServer, $domain, $transactionId, $loginUrl, $loginExpiredUrl, $loginErrorUrl){
        //Client side HTML to display a NOMO qr code
        //and a form for optional device id entry.
        //Javascript to poll the NOMO server for a authentication for the displayed QR code.
        //and to process the submit on the device id entry form.
        $pollUrl = $nomoServer.NOMO_WS_URL_DOMAIN_POLL_INTRODUCTION;
        $AuthRequestUrl = $nomoServer.NOMO_WS_URL_DOMAIN_REQUEST_AUTHENTICATION;

        if (!isset($loginUrl)){
            $loginUrl = "loginprocess.php";
        }
        if (!isset($loginExpiredUrl)){
            $loginExpiredUrl = "loginexpired.php";
        }
        if (!isset($loginErrorUrl)){
            $loginErrorUrl = "loginerror.php";
        }

        $timeout = AUTHENTICATION_POLL_WAIT * 1000;
        $pollRequestData = $this->getAuthenticationPollJson($domain, $_SERVER['REMOTE_ADDR'], $transactionId);
        $loginRequestData = $this->getAuthorizationRequestJson($nomoServer, $domain, $_SERVER['REMOTE_ADDR'], $transactionId);

        $code = '    <script type="text/javascript" src="'.NOMO_SDK_DIRECTORY.'jquery.js"></script>'."\n";
        $code .= '   <script type="text/javascript">'."\n";
        $code .= "\n";
        $code .= '   $(document).ready(function(){NOMO_Poll()});'."\n";
        $code .= "\n";
        $code .= '   <!--NOMO Authentication Polling Processing: -->'."\n";
        $code .= "\n";
        $code .= '       function NOMO_Poll(){'."\n";
        $code .= "           var data = '".$pollRequestData."';"."\n";
        $code .= '               $.ajax({'."\n";
        $code .= '                    url:            "'.$pollUrl. '", '."\n";
        $code .= '                    data:           data,'."\n";
        $code .= '                    type:           "POST",'."\n";
        $code .= '                    contentType:    "application/json",'."\n";
        $code .= '                    timeout:        '.$timeout.','."\n";
        $code .= '                    dataType:       "json",'."\n";
        $code .= '                    success:        NOMO_Poll_Response,'."\n";
        $code .= '                    complete:       NOMO_Poll_Complete'."\n";
        $code .= '                })'."\n";
        $code .= '       }'."\n";
        $code .= "\n";
        $code .= '       function NOMO_Poll_Response(data, textStatus, jqXHR){'."\n";
        $code .= '           var status = data.status;'."\n";
        $code .= '           switch(status){'."\n";
        $code .= '               case '.NOMO_STATUS_TRANSACTION_EXPIRED.': <!--Transaction Expired-->'."\n";
        $code .= '                   window.location.href = "'.$loginExpiredUrl.'";'."\n";
        $code .= '                   break;'."\n";
        $code .= '               case '.NOMO_STATUS_POLL_AUTHENTICATED.': <!--Transaction Authenticated-->'."\n";
        $code .= '                   queryStr = "'. $loginUrl. '?'.NOMO_FIELD_NOMO_ISSUER.'='.$nomoServer.'&'.NOMO_FIELD_TRANSACTION_ID.'='.$transactionId.'"'."\n";
        $code .= '                   window.location.href = queryStr;'."\n";
        $code .= '                   break;'."\n";
        $code .= '               case '.NOMO_STATUS_POLL_NOT_AUTHENTICATED .': <!--Transaction Valid but not authenticated yet-->'."\n";
        $code .= '                   setTimeout(function() {NOMO_Poll()}, 2000);'."\n";
        $code .= '                   break;'."\n";
        $code .= '               default:'." <!--Error Status Received-->\n";
        $code .= '                   window.location.href = getErrorPageUrl("PollRequest NOMO Error", status, jqXHR, textStatus, data);'."\n";
        $code .= '                   break;'."\n";
        $code .= '           }'."\n";
        $code .= '       }'."\n";
        $code .= "\n";
        $code .= '       function NOMO_Poll_Complete(jqXHR, textStatus){'."\n";
        $code .= '           if (textStatus == "error" || !jqXHR.responseText) {'."\n";
        $code .= '               NOMO_Poll_Error(jqXHR, textStatus, jqXHR.responseText);'."\n";
        $code .= '           }'."\n";
        $code .= '       }'."\n";
        $code .= "\n";
        $code .= '       function NOMO_Poll_Error(jqXHR, textStatus, errorThrown){'."\n";
        $code .= '           if (textStatus == "timeout"){'."\n";
        $code .= '               setTimeout(function(){NOMO_Poll()}, 3000);'."\n";
        $code .= '           }else{'."\n";
        $code .= '               if(jqXHR.readyState == 0 || jqXHR.status == 0){'."\n";
        $code .= '                   setTimeout(function(){NOMO_Poll()}, 3000);'."\n";
        $code .= '                   return;  // not really an error '."\n";
        $code .= '               }'."\n";
        $code .= '               window.location.href = getErrorPageUrl("PollRequest AJAX Error", "'.NOMO_STATUS_AJAX_ERROR.'", jqXHR, textStatus, errorThrown);'."\n";
        $code .= '           }'."\n";
        $code .= '       }'."\n";
        $code .= "\n";
        $code .= '   <!--NOMO Authentication Request Processing: -->'."\n";
        $code .= "\n";
        $code .= '       function NOMO_Auth_Request(){'."\n";
        $code .= "           var data = '".$loginRequestData."';"."\n";
        $code .= '           var loginId = $("#nomo_login").val();'."\n";
        $code .= '           data = data.replace("##LoginId##",loginId);'."\n";
        $code .= '           $.ajax({'."\n";
        $code .= '                    url:            "'.$AuthRequestUrl. '", '."\n";
        $code .= '                    data:           data,'."\n";
        $code .= '                    type:           "POST",'."\n";
        $code .= '                    contentType:    "application/json",'."\n";
        $code .= '                    timeout:        '.$timeout.','."\n";
        $code .= '                    dataType:       "json",'."\n";
        $code .= '                    success:        NOMO_Auth_Response,'."\n";
        $code .= '                    complete:       NOMO_Auth_Complete'."\n";
        $code .= '                })'."\n";
        $code .= '       }'."\n";
        $code .= "\n";
        $code .= '       function NOMO_Auth_Response(data, textStatus, jqXHR){'."\n";
        $code .= '           var status = data.status;'."\n";
        $code .= '           switch(status){'."\n";
        $code .= '               case '.NOMO_STATUS_TRANSACTION_EXPIRED.': <!--Transaction Expired-->'."\n";
        $code .= '                   window.location.href = "'.$loginExpiredUrl.'";'."\n";
        $code .= '                   break;'."\n";
        $code .= '               case '.NOMO_STATUS_SUCCESS.': <!--Authentication requested-->'."\n";
        $code .= '                   break;'."\n";
        $code .= '               default:'." <!--Error Status Received-->\n";
        $code .= '                   window.location.href = getErrorPageUrl("AuthenticationRequest", status, jqXHR, textStatus, "");'."\n";
        $code .= '                   break;'."\n";
        $code .= '           }'."\n";
        $code .= '       }'."\n";
        $code .= "\n";
        $code .= '       function NOMO_Auth_Complete(jqXHR, textStatus){'."\n";
        $code .= '           if (textStatus == "error" || !jqXHR.responseText) {'."\n";
        $code .= '               NOMO_Auth_Error("AuthenticationRequest", "", jqXHR, textStatus, "");'."\n";
        $code .= '           }'."\n";
        $code .= '       }'."\n";
        $code .= "\n";
        $code .= '       function NOMO_Auth_Error(jqXHR, textStatus, errorThrown){'."\n";
        $code .= '          window.location.href = getErrorPageUrl("AuthenticationRequest", "'.NOMO_STATUS_AJAX_ERROR.'", jqXHR, textStatus, errorThrown);'."\n";
        $code .= '       }'."\n";
        $code .= "\n";
        $code .= '   <!--NOMO Helper Functions: -->'."\n";
        $code .= '       function getErrorPageUrl(mode, nomoStatus, jqXHR, textStatus, data){'."\n";
        $code .= '           var status = "";'."\n";
        $code .= '           var url = "'.$loginErrorUrl.'?"'."\n";
        $code .= '           url = url + "'.NOMO_FIELD_AJAX_ACTION.'=" + mode;'."\n";
        $code .= '           url = url + "&'.NOMO_FIELD_STATUS.'=" + nomoStatus;'."\n";
        $code .= '           if (jqXHR.status != "") {'."\n";
        $code .= '              status = jqXHR.status;'."\n";
        $code .= '           }'."\n";
        $code .= '           url = url + "&'.NOMO_FIELD_AJAX_STATUS_CODE.'=" + jqXHR.status;'."\n";
        $code .= '           url = url + "&'.NOMO_FIELD_AJAX_STATUS_TEXT.'=" + textStatus;'."\n";
        $code .= '           url = url + "&'.NOMO_FIELD_AJAX_DATA.'=" + data;'."\n";
        $code .= '           url = url + "&'.NOMO_FIELD_AJAX_URL.'=" + "'.$pollUrl.'";'."\n";
        $code .= '           return url;'."\n";
        $code .= '       }'."\n";
        $code .= '   </script>'."\n";
        return $code;
    }

    protected function callNOMOWebservice($url, $requestData, $timeout){

        //Call the NOMO Server web service
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Request URL: ".$url);
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"Request Data: ".$requestData);

        if (!isset($timeout))
            $timeout = NOMO_SERVER_CALL_WAIT;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,            $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $requestData );
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);


        $response=curl_exec ($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $httpError = curl_error ($ch);
        curl_close($ch);

        if($httpcode != 200){
            //HTTP failure...
            $result = '{"status":'.NOMO_STATUS_REQUEST_HTTP_ERROR;
            $result .=',"url":"'.$url.'"';
            $result .=',"httpcode":"'.$httpcode.'"';
            $result .=',"httpError":"'.$httpError.'"';
            $result .=',"response":"'.$response.'"';
            $result .='}';
            $response =  $result;
        }
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,$this->getContext(),"WS Response: $response");

        return $response;
    }

    protected function getNomoServerURL($nomoServer, $webService){
        return trim($nomoServer).trim($webService);
    }

    protected function getRequestConfigJson($domain, $clientIP){
        $result = '{';
        $result .= '"'.NOMO_FIELD_ORIGINATOR.'":"'.$domain.'"';
        $result .= ',"'.NOMO_FIELD_CLIENT_IP.'":"'.$clientIP.'"';
        $result .= '}';
        return $result;
    }

    protected function getQRCodeRequestJson($domain, $clientIP){
        $result = '{';
        $result .= '"'.NOMO_FIELD_ORIGINATOR.'":"'.$domain.'"';
        $result .= ',"'.NOMO_FIELD_CLIENT_IP.'":"'.$clientIP.'"';
        $result .= '}';
        return $result;
    }

    protected function getAuthenticationPollJson($domain, $clientIp, $transactionId){
        $payload = '{';
        $payload .= '"'.NOMO_FIELD_DOMAIN.'":"'.$domain.'"';
        if (isset($transactionId))
            $payload .= ',"'.NOMO_FIELD_TRANSACTION_ID.'":"'.$transactionId.'"';
        $payload .= '}';

        $result = '{';
        $result .= '"'.NOMO_FIELD_ORIGINATOR.'":"'.$domain.'",';
        $result .= '"'.NOMO_FIELD_CLIENT_IP.'":"'.$clientIp.'",';
        $result .= $this->encryptPayload($payload);
        $result .= '}';
        return $result;
    }

    protected function getAuthorizationRequestJson($nomoServer, $domain, $clientIp, $transactionId){
        $payload = '{';
        $payload .= '"'.NOMO_FIELD_EXTERNAL_TASK_ID.'":"'.NOMO_FIELD_VALUE_TASK_ID_LOGIN.'"';
        $payload .= ',"'.NOMO_FIELD_DOMAIN.'":"'.$domain.'"';
        $payload .= ',"'.NOMO_FIELD_DEVICE_ID.'":"##LoginId##"';
        if (isset($nomoServer))
            $payload .= ',"'.NOMO_FIELD_NOMO_ISSUER.'":"'.$nomoServer.'"';
        if (isset($transactionId))
            $payload .= ',"'.NOMO_FIELD_TRANSACTION_ID.'":"'.$transactionId.'"';
        $payload .= '}';

        $result = '{';
        $result .= '"'.NOMO_FIELD_ORIGINATOR.'":"'.$domain.'",';
        $result .= '"'.NOMO_FIELD_CLIENT_IP.'":"'.$clientIp.'",';
        $result .= $this->encryptPayload($payload);
        $result .= '}';
        return $result;
    }

    protected function getLoginRequestJson($domain, $clientIp, $transactionId){
        $payload = '{';
        $payload .= '"'.NOMO_FIELD_DOMAIN.'":"'.$domain.'"';
        if (isset($transactionId))
            $payload .= ',"'.NOMO_FIELD_TRANSACTION_ID.'":"'.$transactionId.'"';
        $payload .= '}';

        $result = '{';
        $result .= '"'.NOMO_FIELD_ORIGINATOR.'":"'.$domain.'",';
        $result .= '"'.NOMO_FIELD_CLIENT_IP.'":"'.$clientIp.'",';
        $result .= $this->encryptPayload($payload);
        $result .= '}';
        return $result;
    }

    protected function getRequestShippingLocalsJson($domain, $clientIp, $deviceId){
        $payload = '{';
        if (isset($domain))
            $payload .= '"'.NOMO_FIELD_DOMAIN.'":"'.$domain.'"';
        if (isset($deviceId))
            $payload .= ',"'.NOMO_FIELD_DEVICE_ID.'":"'.$deviceId.'"';

        $payload .= '}';

        $result = '{';
        $result .= '"'.NOMO_FIELD_ORIGINATOR.'":"'.$domain.'",';
        $result .= '"'.NOMO_FIELD_CLIENT_IP.'":"'.$clientIp.'",';
        $result .= $this->encryptPayload($payload);
        $result .= '}';
        return $result;
    }

    protected function getCheckoutRequestJson(
        $domain,
        $clientIp,
        $deviceId,
        $transactionId,
        nomoCart $cart,
        $shippingOptions=array(),
        $requiredData=array())
    {

        $cart->updateTotals();
        $payload = '{';
        if (isset($domain))
            $payload .= '"'.NOMO_FIELD_DOMAIN.'":"'.$domain.'"';

        if (isset($deviceId))
            $payload .= ',"'.NOMO_FIELD_DEVICE_ID.'":"'.$deviceId.'"';

        if (isset($transactionId))
            $payload .= ',"'.NOMO_FIELD_TRANSACTION_ID.'":"'.$transactionId.'"';

        if (isset($requiredData) && !empty($requiredData)) {
            $req = "";
            $first = true;
            foreach ($requiredData as $item) {
                if ($first){
                    $req .= "[".$item;
                }else{
                    $req .= ",".$item;
                }
                $first = false;
            }
            if (!empty($req)) {
                $payload .= ',"' . NOMO_FIELD_CHECKOUT_REQUIRED_DATA . '":';
                $payload .= $req.']';
            }
        }

        if (isset($shippingOptions) && !empty($shippingOptions)) {
            $req = "";
            $first = true;

            foreach ($shippingOptions as $item) {
                /* @var $item NomoShippingOption */
                if (!$first){
                    $req .= ",";
                }
                $first = false;
                $req .="{";
                $req .='"'.NOMO_FIELD_MERCHANT_SHIPPING_OPTION_COUNTRY.'":"'.$item->getScountry().'"';
                $req .=',"'.NOMO_FIELD_MERCHANT_SHIPPING_OPTION_POSTAL.'":"'.$item->getSpostal().'"';
                $req .=',"'.NOMO_FIELD_MERCHANT_SHIPPING_OPTION_NAME.'":"'.$item->getSoption().'"';
                $req .=',"'.NOMO_FIELD_MERCHANT_SHIPPING_OPTION_COST.'":"'.$item->getScost().'"';
                $req .="}";
            }
            if (!empty($req)) {
                $payload .= ',"' . NOMO_FIELD_MERCHANT_SHIPPING_OPTIONS . '":';
                $payload .= '['.$req.']';
            }
        }

        if (isset($cart)) {
            $payload .= ',"'.NOMO_FIELD_TOTAL.'":'.$cart->getTotal();
            $payload .= ',"'.NOMO_FIELD_CART.'":'.$this->removeMutipleSpaces($cart->toNoMoJson(true));
        }

        $payload .= '}';

        $result = '{';
        $result .= '"'.NOMO_FIELD_ORIGINATOR.'":"'.$domain.'",';
        $result .= '"'.NOMO_FIELD_CLIENT_IP.'":"'.$clientIp.'",';
        $result .= $this->encryptPayload($payload);
        $result .= '}';
        return $result;
    }

    protected function getCheckoutPollRequestJson($domain, $clientIp, $transactionId){
        $payload = '{';
        if (isset($domain))
            $payload .= '"'.NOMO_FIELD_DOMAIN.'":"'.$domain.'"';
        if (isset($transactionId))
            $payload .= ',"'.NOMO_FIELD_TRANSACTION_ID.'":"'.$transactionId.'"';
        $payload .= '}';

        $result = '{';
        $result .= '"'.NOMO_FIELD_ORIGINATOR.'":"'.$domain.'",';
        $result .= '"'.NOMO_FIELD_CLIENT_IP.'":"'.$clientIp.'",';
        $result .= $this->encryptPayload($payload);
        $result .= '}';
        return $result;
    }

    protected function getCheckoutCompleteRequestJson($domain, $clientIp, $deviceId, $transactionId, $transactionStatus, nomoCart $cart){
        $payload = '{';
        if (isset($domain))
            $payload .= '"'.NOMO_FIELD_DOMAIN.'":"'.$domain.'"';
        if (isset($deviceId))
            $payload .= ',"'.NOMO_FIELD_DEVICE_ID.'":"'.$deviceId.'"';
        if (isset($transactionId))
            $payload .= ',"'.NOMO_FIELD_TRANSACTION_ID.'":"'.$transactionId.'"';
        if (isset($transactionStatus))
           $payload .= ',"'.NOMO_FIELD_STATUS.'":'.$transactionStatus.'';
         if (isset($cart)) {
            $payload .= ',"'.NOMO_FIELD_TOTAL.'":"'.$cart->getTotal().'"';
            $payload .= ',"'.NOMO_FIELD_CART.'":'.$cart->toNoMoJson(true).'';
        }
        $payload .= '}';

        $result = '{';
        $result .= '"'.NOMO_FIELD_ORIGINATOR.'":"'.$domain.'",';
        $result .= '"'.NOMO_FIELD_CLIENT_IP.'":"'.$clientIp.'",';
        $result .= $this->encryptPayload($payload);
        $result .= '}';
        return $result;
    }

    private function encryptPayload($payload){
        $result = '"'.NOMO_FIELD_PAYLOAD.'":';
        if (ENCRYPTION_SDK_ON){
            //Encrypt the payload into a string.
            $result .= $payload;
        }else{
            $result .= $payload;
        }
        return $result;
    }

    private function decryptPayload($payload){
        $result = "";
        if (ENCRYPTION_SDK_ON){
            //Decrypt the payload into a string.
            $result = $payload;
        }else{
            $result = $payload;
        }
        return $result;
    }

    protected function IsNullOrEmptyString($question)
    {
        if (!isset($question))
            return true;
        if (is_string($question)) {
            if (empty($question)){
                return true;
            }
        }
        return true;
    }

    public function isMobileBrowser () {
        $detect = new Mobile_Detect();
        if ($detect->isMobile()){
            return true;
        }
        return false;
    }

    public function isNoMoInstalled($url){
        if (!$fp = curl_init($url))
            return false;
        return true;
/*
        $file = $url;
        $file_headers = @get_headers($file);
        if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
             return false;
        }
        return true;
*/
    }

    public function getStringAsHex($data, $asHTML){
        $output = "";
        if (!is_string($data)){
            return;
        }

        $count = 0;
        if ($asHTML)
            $output .= "<table><tr>";

        for ($i = 0; $i < strlen($data); $i++) {
            $count += 1;
            $txt = dechex(ord($data[$i]))." : ".$data[$i]." ";


            if ($asHTML)
                $output.= "<td>$txt</td>";
            else
                $output .=$txt;

            if ($count > 8){
                if ($asHTML)
                    $output .= "</tr><tr>";
                else
                    $output .= "\n";
                $count = 0;
            }
        }

        if ($asHTML)
            $output .= "</tr></table>";
        return $output;
    }

    private function removeMutipleSpaces($data){
        if (!is_string($data)){
            return $data;
        }
        return preg_replace('/\s+/', ' ',$data);
    }
    
    private function getContext(){
        $context = "";
        if (!empty($this->transactionId)){
            $context .= $this->transactionId;
        }
        if (!empty($this->deviceId)){
            $context .= "-".$this->deviceId;
        }
        if (empty($context)){
            $context .= MY_DOMAIN;
        }
        return $context;
    }

}
?>