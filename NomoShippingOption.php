<?php
 
include_once "nomo_server_constants.php";
 
   // Copyright (c) 2014. Togglebolt Software.
   // genereated by C:\ToggleboltSW\Products\NoMo\NoMo-Tools\SDK\generateNomoShippingOption.php on UTC: 2014/11/19 19:44:28
 
class NomoShippingOption {
 
    private $Scity = "";
    private $Sstate = "";
    private $Scountry = "";
    private $Spostal = "";
    private $Soption = "";
    private $Scost = "";

 
    public function setScity($Scity){
        $this->Scity = trim($Scity);
    }

    public function getScity(){
       return $this->Scity;
    }
    public function setSstate($Sstate){
        $this->Sstate = trim($Sstate);
    }

    public function getSstate(){
       return $this->Sstate;
    }
    public function setScountry($Scountry){
        $this->Scountry = trim($Scountry);
    }

    public function getScountry(){
       return $this->Scountry;
    }
    public function setSpostal($Spostal){
        $this->Spostal = trim($Spostal);
    }

    public function getSpostal(){
       return $this->Spostal;
    }
    public function setSoption($Soption){
        $this->Soption = trim($Soption);
    }

    public function getSoption(){
       return $this->Soption;
    }
    public function setScost($Scost){
        $this->Scost = trim($Scost);
    }

    public function getScost(){
       return $this->Scost;
    }

 
    public function isValid(){
        if(empty($this->Sstate))
            return false; 
        if(empty($this->Scity))
            return false;
        if(empty($this->Spostal))
            return false;
        if(empty($this->Scountry))
            return false;
        return true;
    }
 
    public function toNoMoJson($enclosingBraces){
        $result = "";
        try{
            if ($enclosingBraces)
                $result .= "{";
 
            $result .= " ".'"'.NOMO_FIELD_MERCHANT_SHIPPING_OPTION_CITY.'":'.json_encode($this->Scity).'';
            $result .= ",".'"'.NOMO_FIELD_MERCHANT_SHIPPING_OPTION_STATE.'":'.json_encode($this->Sstate).'';
            $result .= ",".'"'.NOMO_FIELD_MERCHANT_SHIPPING_OPTION_COUNTRY.'":'.json_encode($this->Scountry).'';
            $result .= ",".'"'.NOMO_FIELD_MERCHANT_SHIPPING_OPTION_POSTAL.'":'.json_encode($this->Spostal).'';
            $result .= ",".'"'.NOMO_FIELD_MERCHANT_SHIPPING_OPTION_NAME.'":'.json_encode($this->Soption).'';
            $result .= ",".'"'.NOMO_FIELD_MERCHANT_SHIPPING_OPTION_COST.'":'.json_encode($this->Scost).'';

            if ($enclosingBraces)
                $result .= "}";
        }catch (Exception $ex){
        }
        return $result;
   }
   public function fromNoMoJson($nomo_field_option){
       try{
           //Passed contents of the NOMO_FIELD_MERCHANT_SHIPPING_OPTIONS field 
           if (!isset($nomo_field_option)){
               return;
           }
           if (empty($nomo_field_option)){ 
               return;
           }
 
            $this->Scity = $this->extractField($nomo_field_option,NOMO_FIELD_MERCHANT_SHIPPING_OPTION_CITY,"");
            $this->Sstate = $this->extractField($nomo_field_option,NOMO_FIELD_MERCHANT_SHIPPING_OPTION_STATE,"");
            $this->Scountry = $this->extractField($nomo_field_option,NOMO_FIELD_MERCHANT_SHIPPING_OPTION_COUNTRY,"");
            $this->Spostal = $this->extractField($nomo_field_option,NOMO_FIELD_MERCHANT_SHIPPING_OPTION_POSTAL,"");
            $this->Soption = $this->extractField($nomo_field_option,NOMO_FIELD_MERCHANT_SHIPPING_OPTION_NAME,"");
            $this->Scost = $this->extractField($nomo_field_option,NOMO_FIELD_MERCHANT_SHIPPING_OPTION_COST,"");

       }catch(Exception $ex){
 
       }
       return;
    }
 
    public function extractField ($nomoJson, $fieldConstant, $defaultValue){
        try{
            $data = null;
            if (is_string($nomoJson)){
                $data = json_decode($nomoJson, true);
            }else{
                if (is_array($nomoJson))
                    $data = $nomoJson;
            }
            if (!isset($data))
                return $defaultValue;
     
            if(isset( $data[$fieldConstant] ) ){
                $tmp = json_encode($data[$fieldConstant]);
                $tmp1 = trim($tmp, '"'); 
                return $tmp1;
            }
            return $defaultValue;
        }catch(Exception $ex){
            return $defaultValue;
        }
    }
}

?>

