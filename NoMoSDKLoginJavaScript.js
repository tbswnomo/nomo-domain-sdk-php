 
   // Copyright (c) 2014. Togglebolt Software.
   // genereated by C:\ToggleboltSW\Products\NoMo\NoMo-Tools\SDK\generateLoginJavaScript.php on 2014/11/19 19:44:29

//This file contains Java Script code that the NoMo SDK writes to the browser on demand.
//The NoMo SDK resolves all #data# tokens at the time of submission to the browser.

<script type="text/javascript">
	// Constants set by the server
	var NOMO_CLIENT_DIRECTORY="#NOMO_CLIENT_DIRECTORY#"; 

	var WS_FIELD_NOMO_ISSUER="#WS_FIELD_NOMO_ISSUER#";
	var WS_FIELD_TRANSACTION_ID="#WS_FIELD_TRANSACTION_ID#";
	var WS_FIELD_AJAX_ACTION="#WS_FIELD_AJAX_ACTION#";
	var WS_FIELD_AJAX_STATUS_CODE="#WS_FIELD_AJAX_STATUS_CODE#";
	var WS_FIELD_AJAX_STATUS_TEXT="#WS_FIELD_AJAX_STATUS_TEXT#";
	var WS_FIELD_AJAX_DATA="#WS_FIELD_AJAX_DATA#";
	var WS_FIELD_STATUS="#WS_FIELD_STATUS#";

	var WS_LOGIN_ID_CONSTANT="##LoginId##"; // This constant needs to exist to populate in JSP/PHP
	
	var STATUS_TRANSACTION_EXPIRED="#STATUS_TRANSACTION_EXPIRED#";
	var STATUS_POLL_AUTHENTICATED="#STATUS_POLL_AUTHENTICATED#";
	var STATUS_POLL_NOT_AUTHENTICATED="#STATUS_POLL_NOT_AUTHENTICATED#";
	var STATUS_AJAX_ERROR="#STATUS_AJAX_ERROR#";
	var STATUS_SUCCESS="#STATUS_SUCCESS#";
	var WS_FIELD_NOMO_LOGIN_ID_TEXTBOX="#WS_FIELD_NOMO_LOGIN_ID_TEXTBOX#"; // Need to add this to constants.  Appears hardcoded to an ID of nomo_login
	
	
	// Variables set by the server based on function calls
	var pollRequestData="#pollRequestData#";
	var nomoServer="#nomoServer#";
	var pollURL="#pollURL#"; 
	var pollTimeout="#pollTimeout#"; 
	var loginUrl="#loginUrl#";
	var loginExpiredUrl="#loginExpiredUrl#"; 
	var jQueryUrl="#jQueryUrl#";
	var transactionId="#transactionId#";
	var authRequestUrl="#authRequestUrl#";
	var ajaxTimeout="#ajaxTimeout#";
	var loginErrorUrl="#loginErrorUrl#";
</script>

<script type="text/javascript" src=#jqueryUrl#"></script>
<script type="text/javascript">
	<!--  Begin NoMo polling-->
	$(document).ready(function(){NoMo_Poll()});
  
	<!-- NoMo Authentication Polling Processing function-->
    function NoMo_Poll()
    {
    	var data = pollRequestData;
    	    $.ajax
    	    (
    	    		{
    	    			url:			pollURL,
    	    			data:			data,
    	    			type:			"POST"
    	    			contentType:	"application/json",
    	    			timeout:		pollTimeout,
    	    			dataType:       "json",
    	    			success:		NoMo_Poll_Response,
    	    			complete:       NoMo_Poll_Complete
    	    		}
    	    )
    }
  
    // Processes the polling response  
    function NoMo_Poll_Response(data, textStatus, jqXHR)
    {	
		var status = data.status;
		switch(status)
		{
	  		case STATUS_TRANSACTION_EXPIRED <!--Transaction Expired-->
	  			window.location.href=loginExpiredUrl;
	  			break;

	  		case STATUS_POLL_AUTHENTICATED <!--Transaction Authenticated-->
	  			queryStr = loginUrl + '?' 
	  			queryStr += WS_FIELD_NOMO_ISSUER + '='
	  			queryStr += nomoServer + '&'
	  			queryStr += WS_FIELD_TRANSACTION_ID +'='
	  			queryStr += transactionId;
	  			window.location.href = queryStr;
	  			break;
	  	
	  		case STATUS_POLL_NOT_AUTHENTICATED <!--Transaction Valid but not authenticated yet-->
	  			setTimeout(function() {NoMo_Poll()}, 2000);
	  			break;
    		
	  		default: <!--Error Status Received-->
	  			window.location.href = getErrorPageUrl("PollRequest NoMo Error", status, jqXHR, textStatus, data);
	  			break;    
	  }
    }

    function NoMo_Poll_Complete(jqXHR, textStatus)
    {
    	if (textStatus == "error" || !jqXHR.responseText) 
    	{
    		NoMo_Poll_Error(jqXHR, textStatus, jqXHR.responseText);
    	}
    }
 
    function NoMo_Poll_Error(jqXHR, textStatus, errorThrown)
    {
    	if (textStatus == "timeout")
    	{
    		setTimeout(function(){NoMo_Poll()}, 3000);
    		
    	}	
    	else
    	{
    		window.location.href = getErrorPageUrl("PollRequest AJAX Error", STATUS_AJAX_ERROR, jqXHR, textStatus, errorThrown);
    	}
    }

    <!--NoMo Authentication Request Processing: -->
    function NoMo_Auth_Request()
    {
    	var data = loginRequestData;
    	var loginId = document.getElementById(WS_FIELD_NOMO_LOGIN_ID_TEXTBOX).value;
    	data = data.replace(WS_LOGIN_ID_CONSTANT,loginId); 
    	$.ajax
    	(
    		{
    			url: 			authRequestUrl,
    			data:			data,
    			type:			"POST",
    			contentType:    "application/json",
    			timeout:        ajaxTimeout,
    			dataType:		"json",
    			success:        NoMo_Auth_Response,
    			complete:       NoMo_Auth_Complete                
    		}
    	)
    }

    function NoMo_Auth_Response(data, textStatus, jqXHR)
    {
    	var status = data.status;
    	switch(status)
    	{
    		case STATUS_TRANSACTION_EXPIRED: <!--Transaction Expired-->
    			window.location.href=loginExpiredUrl;
    			break;
    		case STATUS_SUCCESS: <!--Authentication requested-->'
    			break;
    		default: <!--Error Status Received-->
    			window.location.href = getErrorPageUrl("AuthenticationRequest", status, jqXHR, textStatus, "");
    			break;
    	}
    }

    function NoMo_Auth_Complete(jqXHR, textStatus)
    {
    	if (textStatus == "error" || !jqXHR.responseText) 
    	{
    		NoMo_Auth_Error("AuthenticationRequest", "", jqXHR, textStatus, "");
    	}
    }

    function NoMo_Auth_Error(jqXHR, textStatus, errorThrown)
    {
    	window.location.href = getErrorPageUrl("AuthenticationRequest", STATUS_AJAX_ERROR, jqXHR, textStatus, errorThrown);
    }

    <!--NoMo Helper Functions: -->
    function getErrorPageUrl(mode, nomoStatus, jqXHR, textStatus, data)
    {
    	var status = "";
    	var url = loginErrorUrl + '?';
    	url += WS_FIELD_AJAX_ACTION + '=' + mode;
    	url += url + '&' + WS_FIELD_STATUS + '=" + nomoStatus;
    	if (jqXHR.status != "")
    	{
    		status = jqXHR.status;
    	}
    
    	url += '&' + WS_FIELD_AJAX_STATUS_CODE + '=" + jqXHR.status;
    	url += '&' + WS_FIELD_AJAX_STATUS_TEXT + '=" + textStatus;
    	url += '&' + WS_FIELD_AJAX_DATA + '=' + data;
    	url += '&' + WS_FIELD_AJAX_URL + '=" + pollUrl;
    	return url;
    }
</script>
