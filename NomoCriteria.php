<?php
 
include_once "nomo_server_constants.php";
 
   // Copyright (c) 2014. Togglebolt Software.
   // genereated by C:\ToggleboltSW\Products\NoMo\NoMo-Tools\SDK\generateNomoCart.php on UTC: 2014/11/19 19:44:27
 
class NomoCriteria {
 
    private $Domain = "";
    private $Datatype = "";
    private $Group = "";
    private $Subgroup = "";
    private $Start = "";
    private $Maxreturn = "";
    private $Direction = "";
    private $Sortdevice = "";
    private $Sortgroup = "";
    private $Sortsubgroup = "";

 
 
    public function __construct($nomo_json) {
        $this->fromNomoJson($nomo_json);
        return;
    }
    public function setDomain($Domain){
        $this->Domain = trim($Domain);
    }

    public function getDomain(){
       return $this->Domain;
    }
    public function setDatatype($Datatype){
        $this->Datatype = trim($Datatype);
    }

    public function getDatatype(){
       return $this->Datatype;
    }
    public function setGroup($Group){
        $this->Group = trim($Group);
    }

    public function getGroup(){
       return $this->Group;
    }
    public function setSubgroup($Subgroup){
        $this->Subgroup = trim($Subgroup);
    }

    public function getSubgroup(){
       return $this->Subgroup;
    }
    public function setStart($Start){
        $this->Start = trim($Start);
    }

    public function getStart(){
       return $this->Start;
    }
    public function setMaxreturn($Maxreturn){
        $this->Maxreturn = trim($Maxreturn);
    }

    public function getMaxreturn(){
       return $this->Maxreturn;
    }
    public function setDirection($Direction){
        $this->Direction = trim($Direction);
    }

    public function getDirection(){
       return $this->Direction;
    }
    public function setSortdevice($Sortdevice){
        $this->Sortdevice = trim($Sortdevice);
    }

    public function getSortdevice(){
       return $this->Sortdevice;
    }
    public function setSortgroup($Sortgroup){
        $this->Sortgroup = trim($Sortgroup);
    }

    public function getSortgroup(){
       return $this->Sortgroup;
    }
    public function setSortsubgroup($Sortsubgroup){
        $this->Sortsubgroup = trim($Sortsubgroup);
    }

    public function getSortsubgroup(){
       return $this->Sortsubgroup;
    }

 
    public function toNoMoJson($enclosingBraces){
        $result = "";
        try{
            if ($enclosingBraces)
                $result .= "{";
 
            $result .= " ".'"'.NOMO_FIELD_CRITERIA_DOMAIN.'":'.json_encode($this->Domain).'';
            $result .= ",".'"'.NOMO_FIELD_CRITERIA_DATA_TYPE.'":'.json_encode($this->Datatype).'';
            $result .= ",".'"'.NOMO_FIELD_CRITERIA_BY_GROUP.'":'.json_encode($this->Group).'';
            $result .= ",".'"'.NOMO_FIELD_CRITERIA_BY_SUBGROUP.'":'.json_encode($this->Subgroup).'';
            $result .= ",".'"'.NOMO_FIELD_CRITERIA_START.'":'.json_encode($this->Start).'';
            $result .= ",".'"'.NOMO_FIELD_CRITERIA_MAX_RETURN.'":'.json_encode($this->Maxreturn).'';
            $result .= ",".'"'.NOMO_FIELD_CRITERIA_DIRECTION.'":'.json_encode($this->Direction).'';
            $result .= ",".'"'.NOMO_FIELD_CRITERIA_SORT_DEVICE.'":'.json_encode($this->Sortdevice).'';
            $result .= ",".'"'.NOMO_FIELD_CRITERIA_SORT_GROUP.'":'.json_encode($this->Sortgroup).'';
            $result .= ",".'"'.NOMO_FIELD_CRITERIA_SORT_SUBGROUP.'":'.json_encode($this->Sortsubgroup).'';

            if ($enclosingBraces)
                $result .= "}";
        }catch (Exception $ex){
        }
        return $result;
   }
   public function fromNomoJson($nomo_field_criteria){
       try{
           //Passed contents of the NOMO_FIELD_CRITERIA field 
           if (!isset($nomo_field_criteria)){
               return;
           }
           if (empty($nomo_field_criteria)){ 
               return;
           }
 
            $this->Domain = $this->extractField($nomo_field_criteria,NOMO_FIELD_CRITERIA_DOMAIN,"");
            $this->Datatype = $this->extractField($nomo_field_criteria,NOMO_FIELD_CRITERIA_DATA_TYPE,"");
            $this->Group = $this->extractField($nomo_field_criteria,NOMO_FIELD_CRITERIA_BY_GROUP,"");
            $this->Subgroup = $this->extractField($nomo_field_criteria,NOMO_FIELD_CRITERIA_BY_SUBGROUP,"");
            $this->Start = $this->extractField($nomo_field_criteria,NOMO_FIELD_CRITERIA_START,"");
            $this->Maxreturn = $this->extractField($nomo_field_criteria,NOMO_FIELD_CRITERIA_MAX_RETURN,"");
            $this->Direction = $this->extractField($nomo_field_criteria,NOMO_FIELD_CRITERIA_DIRECTION,"");
            $this->Sortdevice = $this->extractField($nomo_field_criteria,NOMO_FIELD_CRITERIA_SORT_DEVICE,"");
            $this->Sortgroup = $this->extractField($nomo_field_criteria,NOMO_FIELD_CRITERIA_SORT_GROUP,"");
            $this->Sortsubgroup = $this->extractField($nomo_field_criteria,NOMO_FIELD_CRITERIA_SORT_SUBGROUP,"");

       }catch(Exception $ex){
 
       }
       return;
    }
 
    public function extractField ($nomoJson, $fieldConstant, $defaultValue){
        try{
            $data = null;
            if (is_string($nomoJson)){
                $data = json_decode($nomoJson, true);
            }else{
                if (is_array($nomoJson))
                    $data = $nomoJson;
            }
            if (!isset($data))
                return $defaultValue;
     
            if(isset( $data[$fieldConstant] ) ){
                $tmp = json_encode($data[$fieldConstant]);
                $tmp1 = trim($tmp, '"'); 
                return $tmp1;
            }
            return $defaultValue;
        }catch(Exception $ex){
            return $defaultValue;
        }
    }
}

?>

