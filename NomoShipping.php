<?php
 
include_once "nomo_server_constants.php";
 
   // Copyright (c) 2014. Togglebolt Software.
   // genereated by C:\ToggleboltSW\Products\NoMo\NoMo-Tools\SDK\generateNomoShipping.php on UTC: 2014/11/19 19:44:28
 
class NomoShipping {
 
    private $Attention = "";
    private $Organization = "";
    private $Residential = "";
    private $Street1 = "";
    private $Street2 = "";
    private $Country = "";
    private $City = "";
    private $State = "";
    private $Postalcode = "";

    private $Shipoption = "";
    private $Shipcost = "";

    private $valid = "true";

 
 
    public function __construct($nomo_json) {
        $this->fromNomoJson($nomo_json);
        return;
    }
    public function isValid(){

       return true;

    }

    public function setAttention($Attention){
        $this->Attention = trim($Attention);
    }

    public function getAttention(){
       return $this->Attention;
    }
    public function setOrganization($Organization){
        $this->Organization = trim($Organization);
    }

    public function getOrganization(){
       return $this->Organization;
    }
    public function setResidential($Residential){
        $this->Residential = trim($Residential);
    }

    public function getResidential(){
       return $this->Residential;
    }
    public function setStreet1($Street1){
        $this->Street1 = trim($Street1);
    }

    public function getStreet1(){
       return $this->Street1;
    }
    public function setStreet2($Street2){
        $this->Street2 = trim($Street2);
    }

    public function getStreet2(){
       return $this->Street2;
    }
    public function setCountry($Country){
        $this->Country = trim($Country);
    }

    public function getCountry(){
       return $this->Country;
    }
    public function setCity($City){
        $this->City = trim($City);
    }

    public function getCity(){
       return $this->City;
    }
    public function setState($State){
        $this->State = trim($State);
    }

    public function getState(){
       return $this->State;
    }
    public function setPostalcode($Postalcode){
        $this->Postalcode = trim($Postalcode);
    }

    public function getPostalcode(){
       return $this->Postalcode;
    }

    public function setShipoption($Shipoption){
        $this->Shipoption = trim($Shipoption);
    }

    public function getShipoption(){
       return $this->Shipoption;
    }
    public function setShipcost($Shipcost){
        $this->Shipcost = trim($Shipcost);
    }

    public function getShipcost(){
       return $this->Shipcost;
    }

 
    public function toNoMoJson($enclosingBraces){
        $result = "";
        try{
            if ($enclosingBraces)
                $result .= "{";
 
            $result .= " ".'"'.NOMO_FIELD_ADDRESS_ATTENTION.'":'.json_encode($this->Attention).'';
            $result .= ",".'"'.NOMO_FIELD_ADDRESS_ORGANIZATION.'":'.json_encode($this->Organization).'';
            $result .= ",".'"'.NOMO_FIELD_ADDRESS_RESIDENTIAL.'":'.json_encode($this->Residential).'';
            $result .= ",".'"'.NOMO_FIELD_ADDRESS_STREET1.'":'.json_encode($this->Street1).'';
            $result .= ",".'"'.NOMO_FIELD_ADDRESS_STREET2.'":'.json_encode($this->Street2).'';
            $result .= ",".'"'.NOMO_FIELD_ADDRESS_COUNTRY.'":'.json_encode($this->Country).'';
            $result .= ",".'"'.NOMO_FIELD_ADDRESS_CITY.'":'.json_encode($this->City).'';
            $result .= ",".'"'.NOMO_FIELD_ADDRESS_STATE.'":'.json_encode($this->State).'';
            $result .= ",".'"'.NOMO_FIELD_ADDRESS_ZIP.'":'.json_encode($this->Postalcode).'';

            $result .= " ".'"'.NOMO_FIELD_CHECKOUT_DATA_SHIPPING_OPTION_NAME.'":'.json_encode($this->Shipoption).'';
            $result .= ",".'"'.NOMO_FIELD_CHECKOUT_DATA_SHIPPING_OPTION_COST.'":'.json_encode($this->Shipcost).'';

            if ($enclosingBraces)
                $result .= "}";
        }catch (Exception $ex){
        }
        return $result;
   }
   public function fromNoMoJson($nomo_field_shipping){
       try{
           //Passed contents of the NOMO_FIELD_SHIPPING_ADDRESS field 
           if (!isset($nomo_field_shipping)){
               return;
           }
           if (empty($nomo_field_shipping)){ 
               return;
           }
 
            $this->Attention = $this->extractField($nomo_field_shipping,NOMO_FIELD_ADDRESS_ATTENTION,"");
            $this->Organization = $this->extractField($nomo_field_shipping,NOMO_FIELD_ADDRESS_ORGANIZATION,"");
            $this->Residential = $this->extractField($nomo_field_shipping,NOMO_FIELD_ADDRESS_RESIDENTIAL,"");
            $this->Street1 = $this->extractField($nomo_field_shipping,NOMO_FIELD_ADDRESS_STREET1,"");
            $this->Street2 = $this->extractField($nomo_field_shipping,NOMO_FIELD_ADDRESS_STREET2,"");
            $this->Country = $this->extractField($nomo_field_shipping,NOMO_FIELD_ADDRESS_COUNTRY,"");
            $this->City = $this->extractField($nomo_field_shipping,NOMO_FIELD_ADDRESS_CITY,"");
            $this->State = $this->extractField($nomo_field_shipping,NOMO_FIELD_ADDRESS_STATE,"");
            $this->Postalcode = $this->extractField($nomo_field_shipping,NOMO_FIELD_ADDRESS_ZIP,"");

            $this->Shipoption = $this->extractField($nomo_field_shipping,NOMO_FIELD_CHECKOUT_DATA_SHIPPING_OPTION_NAME,"");
            $this->Shipcost = $this->extractField($nomo_field_shipping,NOMO_FIELD_CHECKOUT_DATA_SHIPPING_OPTION_COST,"");

       }catch(Exception $ex){
 
       }
       return;
    }
 
    public function extractField ($nomoJson, $fieldConstant, $defaultValue){
        try{
            $data = null;
            if (is_string($nomoJson)){
                $data = json_decode($nomoJson, true);
            }else{
                if (is_array($nomoJson))
                    $data = $nomoJson;
            }
            if (!isset($data))
                return $defaultValue;
     
            if(isset( $data[$fieldConstant] ) ){
                $tmp = json_encode($data[$fieldConstant]);
                $tmp1 = trim($tmp, '"'); 
                return $tmp1;
            }
            return $defaultValue;
        }catch(Exception $ex){
            return $defaultValue;
        }
    }
}

?>

