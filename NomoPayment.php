<?php
 
include_once "nomo_server_constants.php";
 
   // Copyright (c) 2014. Togglebolt Software.
   // genereated by C:\ToggleboltSW\Products\NoMo\NoMo-Tools\SDK\generateNomoPayment.php on UTC: 2014/11/19 19:44:28
 
class NomoPayment {
 
    private $Network = "";
    private $Nameoncard = "";
    private $Number = "";
    private $Expiration = "";
    private $Cvc = "";

    private $valid = "true";

 
    private $Attention = "";
    private $Organization = "";
    private $Residential = "";
    private $Street1 = "";
    private $Street2 = "";
    private $Country = "";
    private $City = "";
    private $State = "";
    private $Postalcode = "";

 
 
    public function __construct($nomo_json) {
        $this->fromNomoJson($nomo_json);
        return;
    }
    public function isValid(){

       return true;

    }

    public function setNetwork($Network){
        $this->Network = trim($Network);
    }

    public function getNetwork(){
       return $this->Network;
    }
    public function setNameoncard($Nameoncard){
        $this->Nameoncard = trim($Nameoncard);
    }

    public function getNameoncard(){
       return $this->Nameoncard;
    }
    public function setNumber($Number){
        $this->Number = trim($Number);
    }

    public function getNumber(){
       return $this->Number;
    }
    public function setExpiration($Expiration){
        $this->Expiration = trim($Expiration);
    }

    public function getExpiration(){
       return $this->Expiration;
    }
    public function setCvc($Cvc){
        $this->Cvc = trim($Cvc);
    }

    public function getCvc(){
       return $this->Cvc;
    }

 
    public function setAttention($Attention){
        $this->Attention = trim($Attention);
    }

    public function getAttention(){
       return $this->Attention;
    }
    public function setOrganization($Organization){
        $this->Organization = trim($Organization);
    }

    public function getOrganization(){
       return $this->Organization;
    }
    public function setResidential($Residential){
        $this->Residential = trim($Residential);
    }

    public function getResidential(){
       return $this->Residential;
    }
    public function setStreet1($Street1){
        $this->Street1 = trim($Street1);
    }

    public function getStreet1(){
       return $this->Street1;
    }
    public function setStreet2($Street2){
        $this->Street2 = trim($Street2);
    }

    public function getStreet2(){
       return $this->Street2;
    }
    public function setCountry($Country){
        $this->Country = trim($Country);
    }

    public function getCountry(){
       return $this->Country;
    }
    public function setCity($City){
        $this->City = trim($City);
    }

    public function getCity(){
       return $this->City;
    }
    public function setState($State){
        $this->State = trim($State);
    }

    public function getState(){
       return $this->State;
    }
    public function setPostalcode($Postalcode){
        $this->Postalcode = trim($Postalcode);
    }

    public function getPostalcode(){
       return $this->Postalcode;
    }

 
    public function toNoMoJson($enclosingBraces){
        $result = "";
        try{
            if ($enclosingBraces)
                $result .= "{";
 
            $result .= " ".'"'.NOMO_FIELD_PAYMENT_NETWORK.'":'.json_encode($this->Network).'';
            $result .= ",".'"'.NOMO_FIELD_PAYMENT_NAME_ON_CARD.'":'.json_encode($this->Nameoncard).'';
            $result .= ",".'"'.NOMO_FIELD_PAYMENT_CARD_NUMBER.'":'.json_encode($this->Number).'';
            $result .= ",".'"'.NOMO_FIELD_PAYMENT_EXPIRE_DATE.'":'.json_encode($this->Expiration).'';
            $result .= ",".'"'.NOMO_FIELD_PAYMENT_CVC.'":'.json_encode($this->Cvc).'';

            if ($enclosingBraces)
                $result .= "}";
        }catch (Exception $ex){
        }
        return $result;
   }
   public function fromNomoJson($nomo_field_Payment){
       try{
           //Passed contents of the NOMO_FIELD_CHECKOUT_DATA_PAYMENT field 
           if (!isset($nomo_field_Payment)){
               return;
           }
           if (empty($nomo_field_Payment)){ 
               return;
           }
 
            $this->Network = $this->extractField($nomo_field_Payment,NOMO_FIELD_PAYMENT_NETWORK,"");
            $this->Nameoncard = $this->extractField($nomo_field_Payment,NOMO_FIELD_PAYMENT_NAME_ON_CARD,"");
            $this->Number = $this->extractField($nomo_field_Payment,NOMO_FIELD_PAYMENT_CARD_NUMBER,"");
            $this->Expiration = $this->extractField($nomo_field_Payment,NOMO_FIELD_PAYMENT_EXPIRE_DATE,"");
            $this->Cvc = $this->extractField($nomo_field_Payment,NOMO_FIELD_PAYMENT_CVC,"");

 
            $billingAddr = $this->extractField($nomo_field_Payment,NOMO_FIELD_CHECKOUT_DATA_ADDRESS,"");
            $this->Attention = $this->extractField($billingAddr,NOMO_FIELD_ADDRESS_ATTENTION,"");
            $this->Organization = $this->extractField($billingAddr,NOMO_FIELD_ADDRESS_ORGANIZATION,"");
            $this->Residential = $this->extractField($billingAddr,NOMO_FIELD_ADDRESS_RESIDENTIAL,"");
            $this->Street1 = $this->extractField($billingAddr,NOMO_FIELD_ADDRESS_STREET1,"");
            $this->Street2 = $this->extractField($billingAddr,NOMO_FIELD_ADDRESS_STREET2,"");
            $this->Country = $this->extractField($billingAddr,NOMO_FIELD_ADDRESS_COUNTRY,"");
            $this->City = $this->extractField($billingAddr,NOMO_FIELD_ADDRESS_CITY,"");
            $this->State = $this->extractField($billingAddr,NOMO_FIELD_ADDRESS_STATE,"");
            $this->Postalcode = $this->extractField($billingAddr,NOMO_FIELD_ADDRESS_ZIP,"");

 
       }catch(Exception $ex){
 
       }
       return;
    }
 
    public function extractField ($nomoJson, $fieldConstant, $defaultValue){
        try{
            $data = null;
            if (is_string($nomoJson)){
                $data = json_decode($nomoJson, true);
            }else{
                if (is_array($nomoJson))
                    $data = $nomoJson;
            }
            if (!isset($data))
                return $defaultValue;
     
            if(isset( $data[$fieldConstant] ) ){
                $tmp = json_encode($data[$fieldConstant]);
                $tmp1 = trim($tmp, '"'); 
                return $tmp1;
            }
            return $defaultValue;
        }catch(Exception $ex){
            return $defaultValue;
        }
    }
}

?>

