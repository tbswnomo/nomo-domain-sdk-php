<?php
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."nomo_logger.php";

//NoMo Network topology:
//Set the url address of the NoMo Server Master that you will communicate with
define ('NOMO_MASTER_SERVER_URL',    "http://nomo-server-dev.toggleboltsw.com/server/");

//Set the URL to the directory you installed the NoMo SDK in on your web server.
define ('NOMO_SDK_DIRECTORY',        "http://nomo-server-dev.toggleboltsw.com/sdk/");
//Set the name used to register your domain with the NoMo server.
define ('MY_DOMAIN',                 "nomo-server-dev");

//NoMo Cookie management
//Set the number of days that the NoMo cookie will be alive. Used by the NoMo SDK cookie methods.
define ('NOMO_COOKIE_DURATION', 90);  //Days
//Set the Scope of the cookie. Used by the NoMo SDK cookie methods.
define ('NOMO_COOKIE_SCOPE',    "nomo-server-dev.toggleboltsw.com");  // Site dependent Set to the sites base domian name

//NoMo communication Wait Times
//Set the time for the NoMo SDK to wait for the NoMo server to respond to a sent message.
///If the SDK is experiencing timeouts for messages send to the NoMo Sever then increase the Server Call wait time.
define ('NOMO_SERVER_CALL_WAIT',     10); //seconds
//Set the time the NoMo SDK will poll for a authentication response from the end user.
//If your users are experiencing timeouts before they can reply to introduction requests increase this Poll Wait times.
define ('AUTHENTICATION_POLL_WAIT',  40); //seconds
//Set the time the NoMo SDK will poll for a checkout payment response from the end user.
//If your users are experiencing timeouts before they can reply to payment requests increase this Poll Wait times.
define ('CHECKOUT_POLL_WAIT',        40); //seconds

//NoMo Support settings.
//Only change these upon guidance from NoMo Support
define ('ENCRYPTION_SDK_ON',             false);

//Date TimeZone (Must be set if running under PHP 5.3 or higher)
date_default_timezone_set ('America/New_York');
 
//Logging Options
//Set Log_Level to Logger::OFF to turn off logging.
//Only set logging on for test servers. Logging will impact performance.
$date = new DateTime();
define ('LOG_POST_FIX',     'NomoSDK.log');
$logFileName = dirname(__FILE__).DIRECTORY_SEPARATOR.$date->format('Ymd').LOG_POST_FIX;
define ('LOG_LEVEL',        Logger::DEBUG);
define ('LOG_TYPE',         Logger::LOG_TO_FILE);
define ('LOG_ECHO',         false);
define ('LOG_FILENAME',     $logFileName);
?>