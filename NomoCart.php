<?php
 
include_once "nomo_server_constants.php";
include_once "NomoCartItem.php";
 
   // Copyright (c) 2014. Togglebolt Software.
   // genereated by C:\ToggleboltSW\Products\NoMo\NoMo-Tools\SDK\generateNomoCart.php on UTC: 2014/11/19 19:44:27
 
class NomoCart {
 
    private $Currency = "";
    private $Subtotal = "";
    private $Tax = "";
    private $Fees = "";
    private $Soption = "";
    private $Scost = "";
    private $Feesdesc = "";
    private $Total = "";

    private $cartItems = array();
 
    public function clearCartItems(){
       unset($this->cartItems);
       $this->cartItems = array();
    }
 
    public function addCartItem(NomoCartItem $item){
       if (isset($item))
          array_push($this->cartItems, $item);
    }
 
    public function getCartItems(){
       return $this->cartItems;
    }
 
    public function setCurrency($Currency){
        $this->Currency = trim($Currency);
    }

    public function getCurrency(){
       return $this->Currency;
    }
    public function setSubtotal($Subtotal){
        $this->Subtotal = trim($Subtotal);
    }

    public function getSubtotal(){
       return $this->Subtotal;
    }
    public function setTax($Tax){
        $this->Tax = trim($Tax);
    }

    public function getTax(){
       return $this->Tax;
    }
    public function setFees($Fees){
        $this->Fees = trim($Fees);
    }

    public function getFees(){
       return $this->Fees;
    }
    public function setSoption($Soption){
        $this->Soption = trim($Soption);
    }

    public function getSoption(){
       return $this->Soption;
    }
    public function setScost($Scost){
        $this->Scost = trim($Scost);
    }

    public function getScost(){
       return $this->Scost;
    }
    public function setFeesdesc($Feesdesc){
        $this->Feesdesc = trim($Feesdesc);
    }

    public function getFeesdesc(){
       return $this->Feesdesc;
    }
    public function setTotal($Total){
        $this->Total = trim($Total);
    }

    public function getTotal(){
       return $this->Total;
    }

      public function updateTotals(){
          $subtotal = 0.0;
          $total = 0.00;
          try{
              foreach($this->cartItems as $item) {
                  /* @var $item NomoCartItem */
                  $cost = floatval($item->getCost());
                  $qnt = floatval($item->getQnt());
                          $subtotal += ($cost * $qnt);
              }
              $this->Subtotal = strval($subtotal);
              $total += $subtotal;
              $total += floatval($this->Fees);
              $total += floatval($this->Tax);
              $total += floatval($this->Scost);
              $this->Total = strval($total);
              }catch (Exception $ex){
          }
      }
 
    public function toNoMoJson($enclosingBraces){
        $result = "";
        try{
            if ($enclosingBraces)
                $result .= "{";
 
            $result .= " ".'"'.NOMO_FIELD_CART_ELEMENT_CURRENCY.'":'.json_encode($this->Currency).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ELEMENT_SUBTOTAL.'":'.json_encode($this->Subtotal).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ELEMENT_TAX.'":'.json_encode($this->Tax).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ELEMENT_FEES.'":'.json_encode($this->Fees).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ELEMENT_SHIPPING_OPTION.'":'.json_encode($this->Soption).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ELEMENT_SHIPPING_COST.'":'.json_encode($this->Scost).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ELEMENT_FEES_DESC.'":'.json_encode($this->Feesdesc).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ELEMENT_TOTAL.'":'.json_encode($this->Total).'';

            $itemJson = "";
            foreach($this->cartItems as $item) {
                /* @var $item NomoCartItem */
                if (!empty($itemJson))
                    $itemJson .= ",";
                $itemJson .= $item->toNoMoJson(true);
            }
            $result .= ',"'.NOMO_FIELD_CART_ITEM_LIST.'": [';
            $result .= $itemJson;
            $result .= "]";
 
            if ($enclosingBraces)
                $result .= "}";
        }catch (Exception $ex){
        }
        return $result;
   }
}

?>

