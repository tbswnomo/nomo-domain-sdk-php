<?php
 
include_once "nomo_server_constants.php";
 
   // Copyright (c) 2014. Togglebolt Software.
   // genereated by C:\ToggleboltSW\Products\NoMo\NoMo-Tools\SDK\generateNomoCart.php on UTC: 2014/11/19 19:44:27
 
class NomoCartItem {
 
    private $Shortdescription = "";
    private $Longdescription = "";
    private $Merchantsku = "";
    private $Manufacture = "";
    private $Manufacturesku = "";
    private $Cost = "";
    private $Qnt = "";
    private $Backorderdate = "";
    private $Returndate = "";
    private $Shipdate = "";
    private $Carrier = "";
    private $Tracking = "";

 
    public function setShortdescription($Shortdescription){
        $this->Shortdescription = trim($Shortdescription);
    }

    public function getShortdescription(){
       return $this->Shortdescription;
    }
    public function setLongdescription($Longdescription){
        $this->Longdescription = trim($Longdescription);
    }

    public function getLongdescription(){
       return $this->Longdescription;
    }
    public function setMerchantsku($Merchantsku){
        $this->Merchantsku = trim($Merchantsku);
    }

    public function getMerchantsku(){
       return $this->Merchantsku;
    }
    public function setManufacture($Manufacture){
        $this->Manufacture = trim($Manufacture);
    }

    public function getManufacture(){
       return $this->Manufacture;
    }
    public function setManufacturesku($Manufacturesku){
        $this->Manufacturesku = trim($Manufacturesku);
    }

    public function getManufacturesku(){
       return $this->Manufacturesku;
    }
    public function setCost($Cost){
        $this->Cost = trim($Cost);
    }

    public function getCost(){
       return $this->Cost;
    }
    public function setQnt($Qnt){
        $this->Qnt = trim($Qnt);
    }

    public function getQnt(){
       return $this->Qnt;
    }
    public function setBackorderdate($Backorderdate){
        $this->Backorderdate = trim($Backorderdate);
    }

    public function getBackorderdate(){
       return $this->Backorderdate;
    }
    public function setReturndate($Returndate){
        $this->Returndate = trim($Returndate);
    }

    public function getReturndate(){
       return $this->Returndate;
    }
    public function setShipdate($Shipdate){
        $this->Shipdate = trim($Shipdate);
    }

    public function getShipdate(){
       return $this->Shipdate;
    }
    public function setCarrier($Carrier){
        $this->Carrier = trim($Carrier);
    }

    public function getCarrier(){
       return $this->Carrier;
    }
    public function setTracking($Tracking){
        $this->Tracking = trim($Tracking);
    }

    public function getTracking(){
       return $this->Tracking;
    }

 
    public function toNoMoJson($enclosingBraces){
        $result = "";
        try{
            if ($enclosingBraces)
                $result .= "{";
 
            $result .= " ".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_TITLE.'":'.json_encode($this->Shortdescription).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_DESCRIPTION.'":'.json_encode($this->Longdescription).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_MERCHANT_SKU.'":'.json_encode($this->Merchantsku).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_MANUFACTURE.'":'.json_encode($this->Manufacture).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_MANUFACTURE_SKU.'":'.json_encode($this->Manufacturesku).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_COST.'":'.json_encode($this->Cost).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_QUANTITY.'":'.json_encode($this->Qnt).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_BACK_ORDERED_DATE.'":'.json_encode($this->Backorderdate).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_RETURNED_DATE.'":'.json_encode($this->Returndate).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_SHIP_DATE.'":'.json_encode($this->Shipdate).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_SHIP_CARRIER.'":'.json_encode($this->Carrier).'';
            $result .= ",".'"'.NOMO_FIELD_CART_ITEM_ELEMENT_SHIP_TRACKING.'":'.json_encode($this->Tracking).'';

            if ($enclosingBraces)
                $result .= "}";
        }catch (Exception $ex){
        }
        return $result;
   }
} 
?>
