<?php
include_once "nomo_SDK_lib.php";
	class Logger
	{
		const DEBUG 	= 1;
		const INFO 		= 2;
		const WARN 		= 3;
		const ERROR 	= 4;
		const FATAL 	= 5;
		const OFF 		= 6;

        const LOG_TO_SYSTEM  = 0;
        const LOG_TO_FILE = 3;
		
		private $DateFormat	= "Y-m-d G:i:s.u";
		private $priority = Logger::INFO;
        public static $_instance = null;

        public static function getInstance() {
            if (self::$_instance == null) {
                self::$_instance = new Logger(LOG_LEVEL);
            }
            return self::$_instance;
        }

		public function __construct($priority )
		{
			$this->priority = $priority;
			return;
		}
		
		public function __destruct()
		{
		}
		
		public function LogInfo($file,$line, $function, $context, $msg)
		{
			return $this->Log($file, $line, $function, Logger::INFO, $context, $msg);
		}
		
		public function LogDebug($file,$line, $function, $context, $msg)
		{
			return $this->Log($file, $line , $function, Logger::DEBUG, $context, $msg);
		}
		
		public function LogWarn($file, $line, $function, $context, $msg)
		{
			return $this->Log($file, $line, $function, Logger::WARN, $context, $msg);
		}
		
		public function LogError($file, $line, $function, $context, $msg)
		{
			return $this->Log($file, $line, $function, Logger::ERROR, $context, $msg);
		}

		public function LogFatal($file, $line, $function, $context, $msg)
		{
			return $this->Log($file, $line, $function, Logger::FATAL, $context, $msg);
		}
		
		public function Log($file, $line, $function, $priority, $context, $msg)
		{
            //Remove extra line feeds from msg
            $msg = str_replace(PHP_EOL,null,$msg);

            $out = "ECHO Not Set";
			if ( $this->priority <= $priority )
			{
                $prefix = $this->getMsgPrefix($file, $line, $function, $priority, $context);
                $out = $prefix.",".$msg;
                error_log($out, LOG_TYPE, LOG_FILENAME);
			}else{
                if (LOG_ECHO){
                    $prefix = $this->getMsgPrefix($file, $line, $function, $priority, $context);
                    $out = "PRIORITY SKIPPED::".$prefix.",".$msg;
                }
            }
            return $out;
		}

        private function getMsgPrefix($file, $line, $function, $priority, $context){

            try {
                $out = "";
                $tag = "NOMO";
                $location = $file;
                if (!empty($function)) {
                    $location .= "::" . $function;
                }
                $location .= "(" . $line . ")";
                $levelTxt = $this->getPriorityDisplay($priority);
                $currPage = $this->getCurrentPageName();
                switch (LOG_TYPE) {
                    case Logger::LOG_TO_SYSTEM:
                        $out = $tag . "," . $levelTxt . "," . $location . "," . $context;
                        break;
                    case Logger::LOG_TO_FILE:
                        $t = microtime(true);
                        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
                        $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
                        $time = $d->format($this->DateFormat);
                        $out = "\n" . $tag . "," . $time . "," . $levelTxt . "," . $location . "," . $currPage . "," . $context;
                        break;
                }
            }catch(Exception $ex){
                $out = $ex->getMessage();
            }
            return $out;
        }

		public function getPriorityDisplay( $priority )
		{
            switch( $priority )
            {
                case Logger::INFO:
                    return "INFO";
                case Logger::WARN:
                    return "WARN";
                case Logger::DEBUG:
                    return "DEBUG";
                case Logger::ERROR:
                    return "ERROR";
                case Logger::FATAL:
                    return "FATAL";
                default:
                    return "LOG";
            }
        }

        private function getCurrentPageName() {
            return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
        }
    }
?>