<?php 
   // Copyright (c) 2014. Togglebolt Software.
   // genereated by C:\ToggleboltSW\Products\NoMo\NoMo-Tools\SDK\generateConstants.php on UTC: 2014/11/19 19:44:27
 
    define("NOMO_WS_URL_DEVICE_REGISTRATION", "WS_Device_Register.php");
    define("NOMO_WS_URL_DEVICE_REGISTRATION_UPDATE", "WS_Device_RegistrationUpdate.php");
    define("NOMO_WS_URL_DEVICE_REGISTER_NOTIFICATION", "WS_Device_Register_Notification.php");
    define("NOMO_WS_URL_DEVICE_REQUEST_SUPPORT", "WS_Device_Request_Support.php");
    define("NOMO_WS_URL_DEVICE_FETCH_TRANSACTION_DETAILS", "WS_Device_Fetch_Transaction_Details.php");
    define("NOMO_WS_URL_DEVICE_INTRODUCTION_REQUEST", "WS_Device_Introduction_Request.php");
    define("NOMO_WS_URL_DEVICE_SHIPPING_LOCAL_UPDATE", "WS_Device_Local_Update.php");
    define("NOMO_WS_URL_DEVICE_TEST_ENCRYPTION", "WS_Device_TestEncryption.php");
    define("NOMO_WS_URL_DEVICE_INTRODUCTION_REPLY", "WS_Device_AuthenticateUser.php");
    define("NOMO_WS_URL_DEVICE_CHECKOUT", "WS_Device_AuthenticateCheckout.php");
    define("NOMO_WS_URL_DOMAIN_ASSIGN_ISSUER", "WS_Domain_Assign_Issuer.php");
    define("NOMO_WS_URL_DOMAIN_REGISTER", "WS_Domain_Register.php");
    define("NOMO_WS_URL_DOMAIN_REQUEST_CONFIG", "WS_Domain_Request_Config.php");
    define("NOMO_WS_URL_DOMAIN_REQUEST_INTRODUCTION", "WS_Domain_Request_QRCode.php");
    define("NOMO_WS_URL_DOMAIN_REQUEST_AUTHENTICATION", "WS_Domain_Request_Authentication.php");
    define("NOMO_WS_URL_DOMAIN_POLL_INTRODUCTION", "WS_Domain_Poll_Authentication.php");
    define("NOMO_WS_URL_DOMAIN_FETCH_INTRODUCTION", "WS_Domain_Fetch_Credentials.php");
    define("NOMO_WS_URL_DOMAIN_ACCEPT_INTRODUCTION", "WS_Domain_Login_User.php");
    define("NOMO_WS_URL_DOMAIN_REQUEST_SHIPPING_LOCALS", "WS_Domain_Request_Shipping_Locals.php");
    define("NOMO_WS_URL_DOMAIN_REQUEST_CHECKOUT", "WS_Domain_Request_Checkout.php");
    define("NOMO_WS_URL_DOMAIN_POLL_CHECKOUT", "WS_Domain_Poll_Checkout.php");
    define("NOMO_WS_URL_DOMAIN_CHECKOUT_COMPLETE", "WS_Domain_Request_Checkout_Complete.php");
    define("NOMO_WS_URL_DOMAIN_CHECKOUT_UPDATE", "WS_Domain_Request_Checkout_Update.php");
    define("NOMO_WS_URL_DEVICE_BASE_ENCRYPTION_IV", "5d7adf668cd109c5");
    define("NOMO_WS_URL_DEVICE_BASE_ENCRYPTION_KEY", "9551d6f801f188c9");

 
    define("NOMO_FIELD_STATUS", "status");
    define("NOMO_FIELD_ORIGINATOR", "originator");
    define("NOMO_FIELD_ISSUER_USE", "issuer");
    define("NOMO_FIELD_CLIENT_IP", "clientip");
    define("NOMO_FIELD_IV", "iv");
    define("NOMO_FIELD_KEY", "key");
    define("NOMO_FIELD_DEVICE_ID", "deviceid");
    define("NOMO_FIELD_DEVICE_CLASS", "deviceclass");
    define("NOMO_FIELD_DEVICE_TYPE", "devicetype");
    define("NOMO_FIELD_DEVICE_OS", "deviceos");
    define("NOMO_FIELD_DEVICE_MODEL", "devicemodel");
    define("NOMO_FIELD_DEVICE_OWNER", "owner");
    define("NOMO_FIELD_DEVICE_ALIAS", "userAlias");
    define("NOMO_FIELD_DEVICE_USER", "username");
    define("NOMO_FIELD_DEVICE_NOTIFICATION_ID", "tokenId");
    define("NOMO_FIELD_DEVICE_RECOVERY_EMAIL", "recoveryemail");
    define("NOMO_FIELD_DEVICE_APP_VERSION", "appversion");
    define("NOMO_FIELD_PAYLOAD", "payload");
    define("NOMO_FIELD_RESULTS", "results");
    define("NOMO_FIELD_DOMAIN", "domain");
    define("NOMO_FIELD_DOMAIN_AUTHENTICATION_DATA", "authentication");
    define("NOMO_FIELD_DOMAIN_USER", "username");
    define("NOMO_FIELD_DOMAIN_PASSWORD", "password");
    define("NOMO_FIELD_TRANSACTION_ID", "transactionId");
    define("NOMO_FIELD_TRANSACTION_TYPE", "transType");
    define("NOMO_FIELD_TRANSACTION_STATE_DATE", "stateDate");
    define("NOMO_FIELD_TRANSACTION_STATE", "Tstate");
    define("NOMO_FIELD_TRANSACTION_ERRORS", "terrors");
    define("NOMO_FIELD_TRANSACTION_ERROR", "terror");
    define("NOMO_FIELD_TRANSACTION_STATUS", "Tstatus");
    define("NOMO_FIELD_TIMEZONE", "timezone");
    define("NOMO_FIELD_DETAIL", "detail");
    define("NOMO_FIELD_TOTAL", "paymenttotal");
    define("NOMO_FIELD_ORDER_NUMBER", "ordernumber");
    define("NOMO_FIELD_NOMO_ISSUER", "issuer");
    define("NOMO_FIELD_IMAGE_FILE_URL", "imageurl");
    define("NOMO_FIELD_IMAGE_FILE_DATA", "imagedata");
    define("NOMO_FIELD_QR_DATA", "qrdata");
    define("NOMO_FIELD_DOMAIN_PRICE_ISSUED", "priceissued");
    define("NOMO_FIELD_DOMAIN_PRICE_AUTHENTICATED", "priceauthenticated");
    define("NOMO_FIELD_DOMAIN_PRICE_LOGIN", "pricelogin");
    define("NOMO_FIELD_DOMAIN_MONTHLY_LIMIT", "monthylimit");
    define("NOMO_FIELD_DOMAIN_OPTION_FAST_CHECKOUT", "fastcheckout");
    define("NOMO_FIELD_AJAX_STATUS_CODE", "JQueryStatus");
    define("NOMO_FIELD_AJAX_STATUS_TEXT", "JQueryStatusText");
    define("NOMO_FIELD_AJAX_DATA", "JQueryData");
    define("NOMO_FIELD_AJAX_URL", "JQueryURL");
    define("NOMO_FIELD_AJAX_ACTION", "JQueryAction");
    define("NOMO_FIELD_SERVER_ERROR_TEXT", "errortext");
    define("NOMO_FIELD_EXTERNAL_TASK_ID", "task");
    define("NOMO_FIELD_NOTIFICATION_MESSAGE", "Note");
    define("NOMO_FIELD_NOTIFICATION_APNS_TASK", "A");
    define("NOMO_FIELD_NOTIFICATION_APNS_DOMAIN", "D");
    define("NOMO_FIELD_NOTIFICATION_APNS_ISSUER", "I");
    define("NOMO_FIELD_NOTIFICATION_APNS_TRANSACTION", "T");
    define("NOMO_FIELD_DEVICE_NOTIFICATION_ARN", "awsArn");
    define("NOMO_FIELD_POLL_MESSAGE", "poll");
    define("NOMO_FIELD_AUTHENTICATED", "authenticated");
    define("NOMO_FIELD_SUPPORT_LOG", "logcat");
    define("NOMO_FIELD_SUPPORT_MESSAGE", "scenario");
    define("NOMO_FIELD_SUPPORT_INCIDENT_ID", "incidentid");
    define("NOMO_FIELD_SUPPORT_EMAIL", "supportemail");
    define("NOMO_FIELD_EVENT_TYPE", "event");
    define("NOMO_FIELD_VAULT_DATA_TYPE", "vaulttype");
    define("NOMO_FIELD_INTRODUCTION_CODE", "introcode");
    define("NOMO_FIELD_TEST_ENCRYPTION_DATA1", "data1");
    define("NOMO_FIELD_TEST_ENCRYPTION_DATA2", "data2");
    define("NOMO_FIELD_TEST_ENCRYPTION_DATA3", "data3");
    define("NOMO_FIELD_TEST_ENCRYPTION_DATA4", "data4");
    define("NOMO_FIELD_MSG", "msg");
    define("NOMO_FIELD_MSG_OPTION_LIST", "moptions");
    define("NOMO_FIELD_MSG_OPTION_ENTRY", "moitem");
    define("NOMO_FIELD_MSG_ELEMENT_TITLE", "mtitle");
    define("NOMO_FIELD_MSG_ELEMENT_BODY", "mbody");
    define("NOMO_FIELD_MSG_OPTION_ELEMENT_TYPE", "msgtype");
    define("NOMO_FIELD_MSG_OPTION_ELEMENT_GROUP", "msggroup");
    define("NOMO_FIELD_MSG_OPTION_ELEMENT_TAG", "msgtag");
    define("NOMO_FIELD_MSG_OPTION_ELEMENT_DISPLAY", "msgdiaply");
    define("NOMO_FIELD_MSG_OPTION_ELEMENT_VALUE", "msgvalue");
    define("NOMO_FIELD_CART", "cart");
    define("NOMO_FIELD_CART_ITEM_LIST", "items");
    define("NOMO_FIELD_CART_ITEM_ENTRY", "item");
    define("NOMO_FIELD_CART_ELEMENT_CURRENCY", "currency");
    define("NOMO_FIELD_CART_ELEMENT_SUBTOTAL", "subtotal");
    define("NOMO_FIELD_CART_ELEMENT_TAX", "tax");
    define("NOMO_FIELD_CART_ELEMENT_FEES", "Fees");
    define("NOMO_FIELD_CART_ELEMENT_SHIPPING_OPTION", "soption");
    define("NOMO_FIELD_CART_ELEMENT_SHIPPING_COST", "scost");
    define("NOMO_FIELD_CART_ELEMENT_FEES_DESC", "FeesDesc");
    define("NOMO_FIELD_CART_ELEMENT_TOTAL", "total");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_TITLE", "shortDescription");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_DESCRIPTION", "longDescription");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_MERCHANT_SKU", "merchantSKU");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_MANUFACTURE", "manufacture");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_MANUFACTURE_SKU", "manufactureSKU");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_COST", "cost");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_QUANTITY", "qnt");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_BACK_ORDERED_DATE", "backorderdate");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_RETURNED_DATE", "returndate");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_SHIP_DATE", "shipdate");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_SHIP_CARRIER", "carrier");
    define("NOMO_FIELD_CART_ITEM_ELEMENT_SHIP_TRACKING", "tracking");
    define("NOMO_FIELD_CHECKOUT_DATA", "checkout");
    define("NOMO_FIELD_CHECKOUT_DATA_CONTACT", "contact");
    define("NOMO_FIELD_CHECKOUT_DATA_PAYMENT", "payment");
    define("NOMO_FIELD_CHECKOUT_DATA_SHIPPING", "shipping");
    define("NOMO_FIELD_CHECKOUT_DATA_ADDRESS", "address");
    define("NOMO_FIELD_CHECKOUT_DATA_SHIPPING_OPTION_NAME", "shipOption");
    define("NOMO_FIELD_CHECKOUT_DATA_SHIPPING_OPTION_COST", "shipCost");
    define("NOMO_FIELD_CHECKOUT_DATA_INSTRUCTIONS", "instructions");
    define("NOMO_FIELD_MERCHANT_SHIPPING_OPTIONS", "SOptions");
    define("NOMO_FIELD_MERCHANT_SHIPPING_OPTION_CITY", "SCity");
    define("NOMO_FIELD_MERCHANT_SHIPPING_OPTION_STATE", "SState");
    define("NOMO_FIELD_MERCHANT_SHIPPING_OPTION_COUNTRY", "SCountry");
    define("NOMO_FIELD_MERCHANT_SHIPPING_OPTION_POSTAL", "SPostal");
    define("NOMO_FIELD_MERCHANT_SHIPPING_OPTION_NAME", "SOption");
    define("NOMO_FIELD_MERCHANT_SHIPPING_OPTION_COST", "SCost");
    define("NOMO_FIELD_CHECKOUT_REQUIRED_DATA", "reqData");
    define("NOMO_FIELD_PAYMENT_NETWORK", "network");
    define("NOMO_FIELD_PAYMENT_NAME_ON_CARD", "nameoncard");
    define("NOMO_FIELD_PAYMENT_CARD_NUMBER", "number");
    define("NOMO_FIELD_PAYMENT_EXPIRE_DATE", "expiration");
    define("NOMO_FIELD_PAYMENT_CVC", "CVC");
    define("NOMO_FIELD_PAYPAL_USERNAME", "username");
    define("NOMO_FIELD_PAYPAL_PASSWORD", "password");
    define("NOMO_FIELD_ADDRESS_ATTENTION", "attention");
    define("NOMO_FIELD_ADDRESS_ORGANIZATION", "organization");
    define("NOMO_FIELD_ADDRESS_RESIDENTIAL", "residential");
    define("NOMO_FIELD_ADDRESS_STREET1", "street1");
    define("NOMO_FIELD_ADDRESS_STREET2", "street2");
    define("NOMO_FIELD_ADDRESS_COUNTRY", "country");
    define("NOMO_FIELD_ADDRESS_CITY", "city");
    define("NOMO_FIELD_ADDRESS_STATE", "state");
    define("NOMO_FIELD_ADDRESS_ZIP", "postalcode");
    define("NOMO_FIELD_CONTACT_NAME_FIRST", "fname");
    define("NOMO_FIELD_CONTACT_NAME_LAST", "lname");
    define("NOMO_FIELD_CONTACT_PHONE", "phone");
    define("NOMO_FIELD_CONTACT_EMAIL", "email");
    define("NOMO_FIELD_CRITERIA", "criteria");
    define("NOMO_FIELD_CRITERIA_DOMAIN", "domain");
    define("NOMO_FIELD_CRITERIA_DATA_TYPE", "datatype");
    define("NOMO_FIELD_CRITERIA_BY_GROUP", "group");
    define("NOMO_FIELD_CRITERIA_BY_SUBGROUP", "subgroup");
    define("NOMO_FIELD_CRITERIA_START", "start");
    define("NOMO_FIELD_CRITERIA_MAX_RETURN", "maxreturn");
    define("NOMO_FIELD_CRITERIA_DIRECTION", "direction");
    define("NOMO_FIELD_CRITERIA_SORT_DEVICE", "sortdevice");
    define("NOMO_FIELD_CRITERIA_SORT_GROUP", "sortgroup");
    define("NOMO_FIELD_CRITERIA_SORT_SUBGROUP", "sortsubgroup");
    define("NOMO_FIELD_VALUE_PAYMENT_NETWORK_DEBIT", "DEBIT");
    define("NOMO_FIELD_VALUE_PAYMENT_NETWORK_VISA", "VISA");
    define("NOMO_FIELD_VALUE_PAYMENT_NETWORK_MASTER_CARD", "MASTERCARD");
    define("NOMO_FIELD_VALUE_PAYMENT_NETWORK_AMEX", "AMEX");
    define("NOMO_FIELD_VALUE_PAYMENT_NETWORK_ENROUTE", "EN ROUTE");
    define("NOMO_FIELD_VALUE_PAYMENT_NETWORK_DINERS", "DINERS");
    define("NOMO_FIELD_VALUE_PAYMENT_NETWORK_DISCOVERY", "DISCOVERY");
    define("NOMO_FIELD_VALUE_PAYMENT_NETWORK_PAYPAL", "PAYPAL");
    define("NOMO_FIELD_VALUE_CHECKOUT_REQUIRED_PHONE", 3101);
    define("NOMO_FIELD_VALUE_CHECKOUT_REQUIRED_EMAIL", 3102);
    define("NOMO_FIELD_VALUE_CHECKOUT_REQUIRED_NAME", 3104);
    define("NOMO_FIELD_VALUE_TASK_ID_LOGIN", 1);
    define("NOMO_FIELD_VALUE_TASK_ID_SHIPPING_LOCALS", 2);
    define("NOMO_FIELD_VALUE_TASK_ID_CHECKOUT_REQUEST", 3);
    define("NOMO_FIELD_VALUE_TASK_ID_CHECKOUT_UPDATE", 4);
    define("NOMO_FIELD_VALUE_TASK_ID_MESSAGE", 99);
    define("NOMO_FIELD_TRANSACTION_TYPE_PAYMENT", 1);
    define("NOMO_FIELD_TRANSACTION_TYPE_REQUEST", 2);
    define("NOMO_FIELD_TRANSACTION_STATE_NOT_STARTED", 0);
    define("NOMO_FIELD_TRANSACTION_STATE_STARTED", 1);
    define("NOMO_FIELD_TRANSACTION_STATE_CHECKOUT_FAILED", 2);
    define("NOMO_FIELD_TRANSACTION_STATE_PAYMENT_FAILED", 3);
    define("NOMO_FIELD_TRANSACTION_STATE_COMPLETE", 4);
    define("NOMO_FIELD_VALUE_DEVICE_CLASS_ANDROID", "ANDROID");
    define("NOMO_FIELD_VALUE_DEVICE_CLASS_APPLE", "APPLE");
    define("NOMO_FIELD_VALUE_DEVICE_CLASS_MICROSOFT", "MICROSOFT");
    define("NOMO_FIELD_VALUE_VAULT_TYPE_TRANSACTION", "transactional");
    define("NOMO_FIELD_VALUE_VAULT_TYPE_PRIVATE", "private");
    define("NOMO_FIELD_VALUE_SORT_ASC", "ASC");
    define("NOMO_FIELD_VALUE_SORT_DESC", "DESC");
    define("NOMO_FIELD_VALUE_AUTHENTICATED_ACCEPT", "true");
    define("NOMO_FIELD_VALUE_AUTHENTICATED_DECLINE", "false");
    define("NOMO_FIELD_VALUE_ADDRESS_RESIDENTIAL", "1");
    define("NOMO_FIELD_VALUE_ADDRESS_COMMERCIAL", "0");

 
    define("NOMO_STATUS_SUCCESS", 0);
    define("NOMO_STATUS_FETCH_CRITERIA_MISSING", 8);
    define("NOMO_STATUS_FETCH_CRITERIA_INVALID", 16);
    define("NOMO_STATUS_FETCH_MISSING_ISSUER", 24);
    define("NOMO_STATUS_REQUEST_INVALID_FORMAT", 100);
    define("NOMO_STATUS_REQUEST_HTTP_ERROR", 110);
    define("NOMO_STATUS_REQUEST_TIMEOUT", 120);
    define("NOMO_STATUS_REQUEST_ERROR", 130);
    define("NOMO_STATUS_REQUEST_EMPTY", 140);
    define("NOMO_STATUS_REQUEST_MISSING_ORIGINATOR", 150);
    define("NOMO_STATUS_EMPTY_RESPONSE", 160);
    define("NOMO_STATUS_EMPTY_PAYLOAD", 170);
    define("NOMO_STATUS_INVALID_PAYLOAD", 180);
    define("NOMO_STATUS_AJAX_ERROR", 190);
    define("NOMO_STATUS_TRANSACTION_ID_MISSING", 200);
    define("NOMO_STATUS_TRANSACTION_UNKNOWN", 210);
    define("NOMO_STATUS_TRANSACTION_INVALID_STATE", 220);
    define("NOMO_STATUS_TRANSACTION_EXPIRED", 230);
    define("NOMO_STATUS_TRANSACTION_INVALID_CLIENT", 240);
    define("NOMO_STATUS_TRANSACTION_EMPTY_CART", 250);
    define("NOMO_STATUS_TRANSACTION_INVALID_CART", 260);
    define("NOMO_STATUS_POLL_AUTHENTICATED", 300);
    define("NOMO_STATUS_POLL_DECLINED", 310);
    define("NOMO_STATUS_POLL_NOT_AUTHENTICATED", 320);
    define("NOMO_STATUS_POLL_DATA_AVAILABLE", 330);
    define("NOMO_STATUS_POLL_DATA_NOT_AVAILABLE", 340);
    define("NOMO_STATUS_DOMAIN_NOT_FOUND", 400);
    define("NOMO_STATUS_DOMAIN_SUSPENDED", 410);
    define("NOMO_STATUS_DOMAIN_INACTIVE", 420);
    define("NOMO_STATUS_DOMAIN_INVALID", 430);
    define("NOMO_STATUS_DOMAIN_USERNAME_INVALID", 440);
    define("NOMO_STATUS_DOMAIN_INSUFFICIENT_PRIVILEGES", 450);
    define("NOMO_STATUS_DOMAIN_NOMO_SERVER_MISSING", 460);
    define("NOMO_STATUS_DOMAIN_DUPLICATE", 470);
    define("NOMO_STATUS_DOMAIN_FAST_CHECKOUT_NOT_ALLOWED", 480);
    define("NOMO_STATUS_DEVICE_NOT_FOUND", 500);
    define("NOMO_STATUS_DEVICE_SUSPENDED", 510);
    define("NOMO_STATUS_DEVICE_INACTIVE", 520);
    define("NOMO_STATUS_DEVICE_INVALID", 530);
    define("NOMO_STATUS_DEVICE_DUPLICATE", 540);
    define("NOMO_STATUS_DEVICE_ID_INVALID", 600);
    define("NOMO_STATUS_DEVICE_TYPE_INVALID", 610);
    define("NOMO_STATUS_DEVICE_USERNAME_INVALID", 620);
    define("NOMO_STATUS_DEVICE_VERSION_INVALID", 630);
    define("NOMO_STATUS_DEVICE_NOTIFICATION_ID_INVALID", 640);
    define("NOMO_STATUS_DEVICE_CLASS_INVALID", 650);
    define("NOMO_STATUS_DEVICE_RECOVERY_INVALID", 660);
    define("NOMO_STATUS_DEVICE_OWNER_INVALID", 670);
    define("NOMO_STATUS_DEVICE_ALIAS_INVALID", 680);
    define("NOMO_STATUS_DEVICE_MISSING_LOCAL_DATA", 690);
    define("NOMO_STATUS_ENCRYPTION_EXPIRED", 700);
    define("NOMO_STATUS_ENCRYPTION_INVALID", 710);
    define("NOMO_STATUS_INTRODUCTION_CODE_MISSING", 720);
    define("NOMO_STATUS_NOTIFICATION_INVALID_JSON", 800);
    define("NOMO_STATUS_NOTIFICATION_INVALID_SENDER", 810);
    define("NOMO_STATUS_NOTIFICATION_SERVER_ERR", 820);
    define("NOMO_STATUS_SERVER_ERROR", 999);
    define("NOMO_STATUS_TRANSACTION_CHECKOUT_COMPLETED", 1000);
    define("NOMO_STATUS_TRANSACTION_ITEMS_SHIPPED", 1010);
    define("NOMO_STATUS_TRANSACTION_MISSING_PAYMENT_DATA", 1100);
    define("NOMO_STATUS_TRANSACTION_MISSING_SHIPPING_DATA", 1120);
    define("NOMO_STATUS_TRANSACTION_MISSING_EMAIL_DATA", 1130);
    define("NOMO_STATUS_TRANSACTION_MISSING_PHONE_DATA", 1140);
    define("NOMO_STATUS_TRANSACTION_INVALID_PAYMENT_DATA", 1150);
    define("NOMO_STATUS_TRANSACTION_INVALID_SHIPPING_DATA", 1160);
    define("NOMO_STATUS_TRANSACTION_INVALID_EMAIL_DATA", 1170);
    define("NOMO_STATUS_TRANSACTION_INVALID_PHONE_DATA", 1180);

 
class NomoStatusDescriptions {
    private static $descriptions = array(
    NOMO_STATUS_SUCCESS=>"Success"
    ,NOMO_STATUS_FETCH_CRITERIA_MISSING=>"Fetch criteria missing"
    ,NOMO_STATUS_FETCH_CRITERIA_INVALID=>"Fetch criteria invalid"
    ,NOMO_STATUS_FETCH_MISSING_ISSUER=>"Fetch missing issuer"
    ,NOMO_STATUS_REQUEST_INVALID_FORMAT=>"Request invalid format"
    ,NOMO_STATUS_REQUEST_HTTP_ERROR=>"Request http error"
    ,NOMO_STATUS_REQUEST_TIMEOUT=>"Request timeout"
    ,NOMO_STATUS_REQUEST_ERROR=>"Request error"
    ,NOMO_STATUS_REQUEST_EMPTY=>"Request empty"
    ,NOMO_STATUS_REQUEST_MISSING_ORIGINATOR=>"Request missing originator"
    ,NOMO_STATUS_EMPTY_RESPONSE=>"Empty response"
    ,NOMO_STATUS_EMPTY_PAYLOAD=>"Empty payload"
    ,NOMO_STATUS_INVALID_PAYLOAD=>"Invalid payload"
    ,NOMO_STATUS_AJAX_ERROR=>"Ajax error"
    ,NOMO_STATUS_TRANSACTION_ID_MISSING=>"Transaction id missing"
    ,NOMO_STATUS_TRANSACTION_UNKNOWN=>"Transaction unknown"
    ,NOMO_STATUS_TRANSACTION_INVALID_STATE=>"Transaction invalid state"
    ,NOMO_STATUS_TRANSACTION_EXPIRED=>"Transaction expired"
    ,NOMO_STATUS_TRANSACTION_INVALID_CLIENT=>"Transaction invalid client"
    ,NOMO_STATUS_TRANSACTION_EMPTY_CART=>"Transaction empty cart"
    ,NOMO_STATUS_TRANSACTION_INVALID_CART=>"Transaction invalid cart"
    ,NOMO_STATUS_POLL_AUTHENTICATED=>"Poll authenticated"
    ,NOMO_STATUS_POLL_DECLINED=>"Poll declined"
    ,NOMO_STATUS_POLL_NOT_AUTHENTICATED=>"Poll not authenticated"
    ,NOMO_STATUS_POLL_DATA_AVAILABLE=>"Poll data available"
    ,NOMO_STATUS_POLL_DATA_NOT_AVAILABLE=>"Poll data not available"
    ,NOMO_STATUS_DOMAIN_NOT_FOUND=>"Domain not found"
    ,NOMO_STATUS_DOMAIN_SUSPENDED=>"Domain suspended"
    ,NOMO_STATUS_DOMAIN_INACTIVE=>"Domain inactive"
    ,NOMO_STATUS_DOMAIN_INVALID=>"Domain invalid"
    ,NOMO_STATUS_DOMAIN_USERNAME_INVALID=>"Domain username invalid"
    ,NOMO_STATUS_DOMAIN_INSUFFICIENT_PRIVILEGES=>"Domain insufficient privileges"
    ,NOMO_STATUS_DOMAIN_NOMO_SERVER_MISSING=>"Domain nomo server missing"
    ,NOMO_STATUS_DOMAIN_DUPLICATE=>"Domain duplicate"
    ,NOMO_STATUS_DOMAIN_FAST_CHECKOUT_NOT_ALLOWED=>"Domain fast checkout not allowed"
    ,NOMO_STATUS_DEVICE_NOT_FOUND=>"Device not found"
    ,NOMO_STATUS_DEVICE_SUSPENDED=>"Device suspended"
    ,NOMO_STATUS_DEVICE_INACTIVE=>"Device inactive"
    ,NOMO_STATUS_DEVICE_INVALID=>"Device invalid"
    ,NOMO_STATUS_DEVICE_DUPLICATE=>"Device duplicate"
    ,NOMO_STATUS_DEVICE_ID_INVALID=>"Device id invalid"
    ,NOMO_STATUS_DEVICE_TYPE_INVALID=>"Device type invalid"
    ,NOMO_STATUS_DEVICE_USERNAME_INVALID=>"Device username invalid"
    ,NOMO_STATUS_DEVICE_VERSION_INVALID=>"Device version invalid"
    ,NOMO_STATUS_DEVICE_NOTIFICATION_ID_INVALID=>"Device notification id invalid"
    ,NOMO_STATUS_DEVICE_CLASS_INVALID=>"Device class invalid"
    ,NOMO_STATUS_DEVICE_RECOVERY_INVALID=>"Device recovery invalid"
    ,NOMO_STATUS_DEVICE_OWNER_INVALID=>"Device owner invalid"
    ,NOMO_STATUS_DEVICE_ALIAS_INVALID=>"Device alias invalid"
    ,NOMO_STATUS_DEVICE_MISSING_LOCAL_DATA=>"Device missing local data"
    ,NOMO_STATUS_ENCRYPTION_EXPIRED=>"Encryption expired"
    ,NOMO_STATUS_ENCRYPTION_INVALID=>"Encryption invalid"
    ,NOMO_STATUS_INTRODUCTION_CODE_MISSING=>"Introduction code missing"
    ,NOMO_STATUS_NOTIFICATION_INVALID_JSON=>"Notification invalid json"
    ,NOMO_STATUS_NOTIFICATION_INVALID_SENDER=>"Notification invalid sender"
    ,NOMO_STATUS_NOTIFICATION_SERVER_ERR=>"Notification server err"
    ,NOMO_STATUS_SERVER_ERROR=>"Server error"
    ,NOMO_STATUS_TRANSACTION_CHECKOUT_COMPLETED=>"Transaction checkout completed"
    ,NOMO_STATUS_TRANSACTION_ITEMS_SHIPPED=>"Transaction items shipped"
    ,NOMO_STATUS_TRANSACTION_MISSING_PAYMENT_DATA=>"Transaction missing payment data"
    ,NOMO_STATUS_TRANSACTION_MISSING_SHIPPING_DATA=>"Transaction missing shipping data"
    ,NOMO_STATUS_TRANSACTION_MISSING_EMAIL_DATA=>"Transaction missing email data"
    ,NOMO_STATUS_TRANSACTION_MISSING_PHONE_DATA=>"Transaction missing phone data"
    ,NOMO_STATUS_TRANSACTION_INVALID_PAYMENT_DATA=>"Transaction invalid payment data"
    ,NOMO_STATUS_TRANSACTION_INVALID_SHIPPING_DATA=>"Transaction invalid shipping data"
    ,NOMO_STATUS_TRANSACTION_INVALID_EMAIL_DATA=>"Transaction invalid email data"
    ,NOMO_STATUS_TRANSACTION_INVALID_PHONE_DATA=>"Transaction invalid phone data"
    );

    public static function getDescription($status){
        if (isset(self::$descriptions[$status]))
            return self::$descriptions[$status];
        return '';
    }
}

 
?>
