<?php

    function NOMO_COOKIE_FETCH_ALIAS(){
        $alias = "";
        if (isset($_COOKIE[NOMO_FIELD_DEVICE_ALIAS]))
            $alias = $_COOKIE[NOMO_FIELD_DEVICE_ALIAS];
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,"NOMO_COOKIE_FETCH_ALIAS","Alias: ".$alias);
        return $alias;
    }
    
    function NOMO_COOKIE_FETCH_DEVICE_ID(){
        $deviceId = "";
        if (isset($_COOKIE[NOMO_FIELD_DEVICE_ID]))
            $deviceId = $_COOKIE[NOMO_FIELD_DEVICE_ID];
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,"NOMO_COOKIE_FETCH_DEVICE_ID","Device Id: ".$deviceId);
        return $deviceId;
    }
    
    function NOMO_COOKIE_HAS_DEVICE_ID(){
        $result = false;
        $deviceId = NOMO_COOKIE_FETCH_DEVICE_ID();
        if (!empty($deviceId)){
           $result = true;
        }
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,"NOMO_COOKIE_HAS_DEVICE_ID","result: ".$result);
        return $result;
    }
    
    function NOMO_COOKIE_STORE($key, $value){
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,"NOMO_COOKIE_STORE","Setting Cookie ($key) => $value");
        $lifespan = time()+60*60*24*NOMO_COOKIE_DURATION;
        setcookie($key, $value, $lifespan, '/', NOMO_COOKIE_SCOPE,false);
    }
    
    function NOMO_COOKIE_CLEAR(){
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,"NOMO_COOKIE_CLEAR","Starting");
        unset ($_COOKIE[NOMO_FIELD_DEVICE_ID]);
        unset ($_COOKIE[NOMO_FIELD_DEVICE_ALIAS]);
        setcookie(NOMO_FIELD_DEVICE_ID, "", time()-300, '/', NOMO_COOKIE_SCOPE,false);
        setcookie(NOMO_FIELD_DEVICE_ALIAS, "", time()-300, '/', NOMO_COOKIE_SCOPE,false);
    }
    
    function NOMO_SESSION_CLEAR($clearCart){
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,"NOMO_SESSION_CLEAR","Starting");

        if ($clearCart)
            $_SESSION[ShoppingCart] = null;
        unset ($_SESSION[NOMO_FIELD_NOMO_ISSUER]);
        unset ($_SESSION[NOMO_FIELD_TRANSACTION_ID]);
        unset ($_SESSION[NOMO_FIELD_DEVICE_ID]);
        $_SESSION[NOMO_FIELD_TRANSACTION_STATE] = NOMO_FIELD_TRANSACTION_STATE_STARTED;
    }
    
    function NOMO_SESSION_SET_TRANSACTION_STATE($status){
        Logger::getInstance()->LogInfo(__FILE__,__LINE__,__FUNCTION__,"NOMO_SESSION_SET_TRANSACTION_STATE","Setting NOMO_FIELD_TRANSACTION_STATE to ".$status);
        $_SESSION[NOMO_FIELD_TRANSACTION_STATE] = $status;
    }
    
    function NOMO_SESSION_CHECKOUT_CANCEL(){
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,"NOMO_SESSION_CHECKOUT_CANCEL","Cancel Checkout");
        NOMO_SESSION_SET_TRANSACTION_STATE(NOMO_FIELD_TRANSACTION_STATE_NOT_STARTED);
    }
    
    function NOMO_SESSION_CHECKOUT_START(){
        Logger::getInstance()->LogDebug(__FILE__,__LINE__,__FUNCTION__,"NOMO_SESSION_CHECKOUT_START","start Checkout");
        NOMO_SESSION_SET_TRANSACTION_STATE(NOMO_FIELD_TRANSACTION_STATE_STARTED);
    }
    
    function NOMO_SESSION_IS_CHECKOUT_IN_PROGRESS(){
        if (isset($_SESSION[NOMO_FIELD_TRANSACTION_STATE])){
            $status = $_SESSION[NOMO_FIELD_TRANSACTION_STATE];
            switch($status){
                case NOMO_FIELD_TRANSACTION_STATE_STARTED:
                    return true;
    
            }
        }
        return false;
    }






