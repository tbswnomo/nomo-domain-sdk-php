<?php
ini_set('display_errors', 'On');
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."nomo_server_constants.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."nomo_SDK_configuration.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."nomo_SDK.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."nomo_logger.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."NomoCookieUtilities.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."NomoCart.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."NomoCartItem.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."NomoContact.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."NomoPayment.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."NomoShipping.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."NomoShippingOption.php";
include_once  dirname(__FILE__).DIRECTORY_SEPARATOR."Mobile_Detect.php";

